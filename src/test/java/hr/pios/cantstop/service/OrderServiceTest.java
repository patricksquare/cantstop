package hr.pios.cantstop.service;

import hr.pios.cantstop.converter.OrderProductConverter;
import hr.pios.cantstop.dto.*;
import hr.pios.cantstop.exception.NotFoundException;
import hr.pios.cantstop.exception.OrderCreationException;
import hr.pios.cantstop.model.*;
import hr.pios.cantstop.repository.OrderProductRepository;
import hr.pios.cantstop.repository.OrderRepository;
import hr.pios.cantstop.repository.ProductRepository;
import hr.pios.cantstop.repository.UserRepository;
import hr.pios.cantstop.service.impl.OrderServiceImpl;
import hr.pios.cantstop.specification.OrderProductSpecification;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private OrderProductConverter orderProductConverter;

    @Mock
    private LoyaltyPointsService loyaltyPointsService;

    @Mock
    private OrderProductRepository orderProductRepository;

    @Mock
    private NotificationEmailService notificationEmailService;


    @Test
    public void shouldGetAllOrders() {
        UserDto userDto = UserDto.builder().id(1L).username("admin").password("pass").firstName("Admin")
                .lastName("Admin").dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .roleDto(new RoleDto(2L, "ROLE_ADMIN")).build();
        User user = User.builder().id(1L).username("admin").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .role(new Role(2L, "ROLE_ADMIN")).build();
        OrderDto orderDto = OrderDto.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1)
                .totalPrice(new BigDecimal(1)).date(LocalDate.of(2020, 2, 2)).userDto(userDto).build();
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1))
                .categoryDto(new CategoryDto(1L, "category")).subcategoryDto(new SubcategoryDto(1L, "subcategory")).build();
        OrderProductDto orderProductDto = OrderProductDto.builder().orderDto(orderDto).productMap(Map.of(productDto, 0)).build();
        Page<OrderProductDto> expectedOrdersDto = new PageImpl<>(List.of(orderProductDto));

        Order order = Order.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1).totalPrice(new BigDecimal(1))
                .date(LocalDate.of(2020, 2, 2)).user(user).build();
        Product product = Product.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1))
                .category(new Category(1L, "category")).subcategory(new Subcategory(1L, "subcategory")).build();
        OrderProductId orderProductId = OrderProductId.builder().order(order).product(product).build();
        OrderProduct orderProduct = OrderProduct.builder().orderProductId(orderProductId).quantity(1).build();

        Page<OrderProduct> expectedOrders = new PageImpl<>(List.of(orderProduct));
        FilterOrderDto filter = new FilterOrderDto();

        when(orderProductRepository.findAll(any(OrderProductSpecification.class), eq(PageRequest.of(0, 1))))
                .thenReturn(expectedOrders);
        when(orderProductConverter.convert(orderProduct)).thenReturn(orderProductDto);

        Page<OrderProductDto> expectedResponse = orderService.getAllOrders(0, 1, filter);

        assertThat(expectedResponse).isEqualTo(expectedOrdersDto);
    }

    @Test
    public void shouldGetAllOrdersFromCurrentUser() {
        UserDto userDto = UserDto.builder().id(1L).username("admin").password("pass").firstName("Admin")
                .lastName("Admin").dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .roleDto(new RoleDto(2L, "ROLE_ADMIN")).build();
        User user = User.builder().id(1L).username("admin").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .role(new Role(2L, "ROLE_ADMIN")).build();
        OrderDto orderDto = OrderDto.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1).totalPrice(new BigDecimal(1))
                .date(LocalDate.of(2020, 2, 2)).userDto(userDto).build();
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1))
                .categoryDto(new CategoryDto(1L, "category")).subcategoryDto(new SubcategoryDto(1L, "subcategory")).build();
        OrderProductDto orderProductDto = OrderProductDto.builder().orderDto(orderDto).productMap(Map.of(productDto, 0)).build();
        Page<OrderProductDto> expectedOrdersDto = new PageImpl<>(List.of(orderProductDto));

        Order order = Order.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1).totalPrice(new BigDecimal(1))
                .date(LocalDate.of(2020, 2, 2)).user(user).build();
        Product product = Product.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1))
                .category(new Category(1L, "category")).subcategory(new Subcategory(1L, "subcategory")).build();
        OrderProductId orderProductId = OrderProductId.builder().order(order).product(product).build();
        OrderProduct orderProduct = OrderProduct.builder().orderProductId(orderProductId).quantity(1).build();

        Page<OrderProduct> expectedOrders = new PageImpl<>(List.of(orderProduct));
        FilterOrderDto filter = new FilterOrderDto();

        when(orderProductRepository.findAll(any(OrderProductSpecification.class), eq(PageRequest.of(0, 1))))
                .thenReturn(expectedOrders);
        when(orderProductConverter.convert(orderProduct)).thenReturn(orderProductDto);
        Page<OrderProductDto> expectedResponse = orderService.getAllOrders(0, 1, filter);

        assertThat(expectedResponse).isEqualTo(expectedOrdersDto);
    }

    @Test
    public void shouldCreateOrder() {
        User user = User.builder().id(1L).username("admin").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .role(new Role(2L, "ROLE_ADMIN")).build();
        UserDto userDto = UserDto.builder().id(1L).username("admin").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .roleDto(new RoleDto(2L, "ROLE_ADMIN")).build();
        Order order = Order.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1).finalPrice(new BigDecimal(1))
                .totalPrice(new BigDecimal(1)).date(LocalDate.of(2020, 2, 2)).user(user).build();
        OrderDto orderDto = OrderDto.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1).finalPrice(new BigDecimal(1))
                .totalPrice(new BigDecimal(1)).date(LocalDate.of(2020, 2, 2)).userDto(userDto).build();
        Product product = Product.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1))
                .category(new Category(1L, "category")).subcategory(new Subcategory(1L, "subcategory")).build();
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description")
                .price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "category"))
                .subcategoryDto(new SubcategoryDto(1L, "subcategory")).build();
        OrderProductId orderProductId = OrderProductId.builder().order(order).product(product).build();
        OrderProduct orderProduct = OrderProduct.builder().orderProductId(orderProductId).quantity(1).build();
        OrderProductDto orderProductDto = OrderProductDto.builder().orderDto(orderDto).productMap(Map.of(productDto, 0)).build();
        CreateOrderProductDto createOrderProductDto = CreateOrderProductDto.builder().productMap(Map.of(1L, 1))
                .userId(1L).discount(1).build();
        List<OrderProductDto> orders = List.of(orderProductDto);

        when(loyaltyPointsService.hasEnoughLoyaltyPoints(1L, 1)).thenReturn(true);
        when(productRepository.findById(1L)).thenReturn(Optional.of(product));
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        when(orderProductConverter.convert(orderProduct)).thenReturn(orderProductDto);
        when(orderRepository.save(any(Order.class))).thenReturn(order);
        when(orderProductRepository.save(any(OrderProduct.class))).thenReturn(orderProduct);
        when(orderProductRepository.findAllByOrderProductId_Order_Id(order.getId())).thenReturn(List.of(orderProduct));

        List<OrderProductDto> expectedResponse = orderService.createOrder(createOrderProductDto);

        assertThat(expectedResponse).isEqualTo(orders);
    }

    @Test
    public void shouldFailCreateOrderWhenUserHasNotEnoughLoyaltyPoints() {
        CreateOrderProductDto createOrderProductDto = CreateOrderProductDto.builder().productMap(Map.of(1L, 1))
                .userId(1L).discount(1).build();

        when(loyaltyPointsService.hasEnoughLoyaltyPoints(1L, 1)).thenReturn(false);

        try {
            orderService.createOrder(createOrderProductDto);
            failBecauseExceptionWasNotThrown(OrderCreationException.class);
        }
        catch (OrderCreationException e) {
            assertThat(e).hasMessage("User does not have enough loyalty points.");
        }
    }

    @Test
    public void shouldDeleteOrder() {
        when(orderRepository.existsById(1L)).thenReturn(true);

        orderService.deleteOrder(1L);
        verify(orderRepository, times(1)).deleteById(1L);
    }

    @Test
    public void shouldFailToDeleteOrderWhenIdNotExist() {
        when(orderRepository.existsById(1L)).thenReturn(false);

        try {
            orderService.deleteOrder(1L);
            failBecauseExceptionWasNotThrown(NotFoundException.class);
        }
        catch (NotFoundException e) {
            assertThat(e).hasMessage("Order with ID 1 not found.");
        }
    }


}
