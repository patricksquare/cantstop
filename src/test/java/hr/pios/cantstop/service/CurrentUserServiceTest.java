package hr.pios.cantstop.service;

import hr.pios.cantstop.model.Role;
import hr.pios.cantstop.model.User;
import hr.pios.cantstop.repository.UserRepository;
import hr.pios.cantstop.service.impl.CurrentUserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CurrentUserServiceTest {

    @InjectMocks
    private CurrentUserServiceImpl currentUserService;

    @Mock
    private UserRepository userRepository;


    @Test
    public void shouldGetLoggedInRole() {
        Role admin = new Role(2L, "ROLE_ADMIN");

        setAuthenticationForRole(admin);

        assertThat(currentUserService.getLoggedInUserRole()).isEqualTo(admin);
    }

    @Test
    public void shouldGetLoggedInUser() {
        Role admin = new Role(2L, "ROLE_ADMIN");
        User user = User.builder().id(1L).firstName("David").lastName("Pilipovic").username("david.pilipovic")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("david.pilipovic@kaiba.corp")
                .role(admin).build();
        setAuthenticationForRole(admin);

        when(userRepository.findByUsername("david.pilipovic")).thenReturn(user);

        assertThat(currentUserService.getLoggedInUser()).isEqualTo(user);
    }

    @Test
    public void shouldGetLoggedInUserUsername() {
        Role admin = new Role(2L, "ROLE_ADMIN");
        setAuthenticationForRole(admin);

        assertThat(currentUserService.getLoggedInUserUsername()).isEqualTo("david.pilipovic");
    }

    @Test
    public void shouldGetTrueWhenCheckingIsAdmin() {
        Role admin = new Role(2L, "ROLE_ADMIN");
        User user = User.builder().id(1L).firstName("David").lastName("Pilipovic").username("david.pilipovic")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("david.pilipovic@kaiba.corp")
                .role(admin).build();
        setAuthenticationForRole(admin);

        when(userRepository.findByUsername("david.pilipovic")).thenReturn(user);

        assertThat(currentUserService.isLoggedInUserAdmin()).isEqualTo(true);
    }

    @Test
    public void shouldGetFalseWhenCheckingIsAdmin() {
        Role roleUser = new Role(1L, "ROLE_USER");

        setAuthenticationForRole(roleUser);

        assertThat(currentUserService.isLoggedInUserAdmin()).isEqualTo(false);
    }

    private void setAuthenticationForRole(Role userRole) {
        User user = User.builder().id(1L).firstName("David").lastName("Pilipovic").username("david.pilipovic")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("david.pilipovic@kaiba.corp")
                .role(userRole).build();

        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);

        when(authentication.getName()).thenReturn("david.pilipovic");
        when(userRepository.findByUsername(authentication.getName())).thenReturn(user);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }
}
