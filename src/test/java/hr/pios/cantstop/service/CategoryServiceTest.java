package hr.pios.cantstop.service;

import hr.pios.cantstop.converter.CategoryConverter;
import hr.pios.cantstop.dto.CategoryDto;
import hr.pios.cantstop.exception.CategoryCreationException;
import hr.pios.cantstop.exception.NotFoundException;
import hr.pios.cantstop.model.Category;
import hr.pios.cantstop.repository.CategoryRepository;
import hr.pios.cantstop.service.impl.CategoryServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import java.util.List;


@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceTest {

    @InjectMocks
    private CategoryServiceImpl categoryService;

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private CategoryConverter categoryConverter;

    @Test
    public void shouldGetAllCategories() {
        Category category = Category.builder().id(1L).name("categoryTest").build();
        CategoryDto categoryDto = CategoryDto.builder().id(1L).name("categoryTest").build();
        List<Category> categories = List.of(category);
        List<CategoryDto> expectedCategories = List.of(categoryDto);

        when(categoryRepository.findAll()).thenReturn(categories);
        when(categoryConverter.convert(category)).thenReturn(categoryDto);

        List<CategoryDto> responseCategoryDtos = categoryService.getAllCategories();

        assertThat(responseCategoryDtos).isEqualTo(expectedCategories);
    }

    @Test
    public void shouldCreateCategoryAndReturnDto() {
        Category category = Category.builder().id(1L).name("categoryTest").build();
        CategoryDto categoryDto = CategoryDto.builder().id(1L).name("categoryTest").build();

        when(categoryConverter.convert(category)).thenReturn(categoryDto);
        when(categoryConverter.convert(categoryDto)).thenReturn(category);
        when(categoryRepository.save(category)).thenReturn(category);
        when(categoryRepository.existsByName("categoryTest")).thenReturn(false);

        CategoryDto createDto = categoryService.createCategory(categoryDto);

        assertThat(createDto).isEqualTo(categoryDto);
    }

    @Test
    public void shouldFailToCreateCategoryWhenNameExists() {
        CategoryDto categoryDto = CategoryDto.builder().id(1L).name("categoryTest").build();

        when(categoryRepository.existsByName("categoryTest")).thenReturn(true);

        try {
            categoryService.createCategory(categoryDto);
            failBecauseExceptionWasNotThrown(CategoryCreationException.class);
        }
        catch (CategoryCreationException e) {
            assertThat(e).hasMessage("Category with name categoryTest already exists");
        }
    }

    @Test
    public void shouldUpdateCategoryAndReturnDto() {
        Category category = Category.builder().id(1L).name("categoryTest").build();
        CategoryDto categoryDto = CategoryDto.builder().id(1L).name("categoryTest").build();

        when(categoryConverter.convert(categoryDto)).thenReturn(category);
        when(categoryConverter.convert(category)).thenReturn(categoryDto);
        when(categoryRepository.existsById(1L)).thenReturn(true);
        when(categoryRepository.existsByName("categoryTest")).thenReturn(false);
        when(categoryRepository.save(category)).thenReturn(category);

        CategoryDto updateDto = categoryService.updateCategory(categoryDto);

        assertThat(updateDto).isEqualTo(categoryDto);
    }

    @Test
    public void shouldFailToUpdateCategoryWhenIdNotExists() {
        CategoryDto categoryDto = CategoryDto.builder().id(1L).name("categoryTest").build();

        when(categoryRepository.existsById(1L)).thenReturn(false);

        try {
            categoryService.updateCategory(categoryDto);
            failBecauseExceptionWasNotThrown(NotFoundException.class);
        }
        catch (NotFoundException e) {
            assertThat(e).hasMessage("Category with ID 1 not found.");
        }
    }

    @Test
    public void shouldFailToUpdateCategoryWhenNameExists() {
        CategoryDto categoryDto = CategoryDto.builder().id(1L).name("categoryTest").build();

        when(categoryRepository.existsById(1L)).thenReturn(true);
        when(categoryRepository.existsByName("categoryTest")).thenReturn(true);

        try {
            categoryService.updateCategory(categoryDto);
            failBecauseExceptionWasNotThrown(CategoryCreationException.class);
        }
        catch (CategoryCreationException e) {
            assertThat(e).hasMessage("Category with name categoryTest already exists");
        }
    }

    @Test
    public void shouldDeleteCategory() {
        when(categoryRepository.existsById(1L)).thenReturn(true);

        categoryService.deleteCategoryById(1L);
        verify(categoryRepository, times(1)).deleteById(1L);
    }

    @Test
    public void shouldFailToDeleteCategoryWhenIdNotExist() {
        when(categoryRepository.existsById(1L)).thenReturn(false);

        try {
            categoryService.deleteCategoryById(1L);
            failBecauseExceptionWasNotThrown(NotFoundException.class);
        }
        catch (NotFoundException e) {
            assertThat(e).hasMessage("Category with ID 1 not found.");
        }
    }

}