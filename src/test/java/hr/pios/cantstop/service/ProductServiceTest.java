package hr.pios.cantstop.service;

import hr.pios.cantstop.converter.ProductConverter;
import hr.pios.cantstop.dto.CategoryDto;
import hr.pios.cantstop.dto.FilterProductDto;
import hr.pios.cantstop.dto.ProductDto;
import hr.pios.cantstop.dto.SubcategoryDto;
import hr.pios.cantstop.exception.NotFoundException;
import hr.pios.cantstop.exception.ProductCreationException;
import hr.pios.cantstop.model.Category;
import hr.pios.cantstop.model.Product;
import hr.pios.cantstop.model.Subcategory;
import hr.pios.cantstop.repository.CategoryRepository;
import hr.pios.cantstop.repository.ProductRepository;
import hr.pios.cantstop.repository.SubcategoryRepository;
import hr.pios.cantstop.service.impl.ProductServiceImpl;
import hr.pios.cantstop.specification.ProductSpecification;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import java.math.BigDecimal;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @InjectMocks
    private ProductServiceImpl productService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private SubcategoryRepository subcategoryRepository;

    @Mock
    private ProductConverter productConverter;


    @Test
    public void shouldGetAllProducts() {
        Product product = Product.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1)).category(new Category(1L, "category")).subcategory(new Subcategory(1L, "subcategory")).build();
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "categoryDto")).subcategoryDto(new SubcategoryDto(1L, "subcategoryDto")).build();
        Page<Product> products = new PageImpl<>(List.of(product));
        Page<ProductDto> expectedProducts = new PageImpl<>(List.of(productDto));

        when(productRepository.findAll(any(ProductSpecification.class), eq(PageRequest.of(0, 1)))).thenReturn(products);
        when(productConverter.convert(product)).thenReturn(productDto);

        Page<ProductDto> productDtos = productService.getAllProducts(0, 1,
                new FilterProductDto());


        assertThat(productDtos).isEqualTo(expectedProducts);
    }

    @Test
    public void shouldCreateProductAndReturnDto() {
        Product product = Product.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1)).category(new Category(1L, "category")).subcategory(new Subcategory(1L, "subcategory")).build();
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "categoryDto")).subcategoryDto(new SubcategoryDto(1L, "subcategoryDto")).build();

        when(productConverter.convert(product)).thenReturn(productDto);
        when(productConverter.convert(productDto)).thenReturn(product);
        when(productRepository.save(product)).thenReturn(product);
        when(productRepository.existsByName("productTest")).thenReturn(false);
        when(categoryRepository.existsById(1L)).thenReturn(true);
        when(subcategoryRepository.existsById(1L)).thenReturn(true);

        ProductDto createDto = productService.createProduct(productDto);

        assertThat(createDto).isEqualTo(productDto);
    }

    @Test
    public void shouldFailToCreateProductWhenNameExists() {
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "categoryDto")).subcategoryDto(new SubcategoryDto(1L, "subcategoryDto")).build();

        when(productRepository.existsByName("productTest")).thenReturn(true);

        try {
            productService.createProduct(productDto);
            failBecauseExceptionWasNotThrown(ProductCreationException.class);
        }
        catch (ProductCreationException e) {
            assertThat(e).hasMessage("Product with name productTest already exists");
        }
    }

    @Test
    public void shouldUpdateProductAndReturnDto() {
        Product product = Product.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1)).category(new Category(1L, "category")).subcategory(new Subcategory(1L, "subcategory")).build();
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "categoryDto")).subcategoryDto(new SubcategoryDto(1L, "subcategoryDto")).build();

        when(productConverter.convert(productDto)).thenReturn(product);
        when(productConverter.convert(product)).thenReturn(productDto);
        when(productRepository.existsById(1L)).thenReturn(true);
        when(productRepository.existsByName("productTest")).thenReturn(false);
        when(categoryRepository.existsById(1L)).thenReturn(true);
        when(subcategoryRepository.existsById(1L)).thenReturn(true);
        when(productRepository.save(product)).thenReturn(product);

        ProductDto updateDto = productService.updateProduct(productDto);

        assertThat(updateDto).isEqualTo(productDto);
    }

    @Test
    public void shouldFailToUpdateProductWhenIdNotExists() {
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "categoryDto")).subcategoryDto(new SubcategoryDto(1L, "subcategoryDto")).build();

        when(productRepository.existsById(1L)).thenReturn(false);

        try {
            productService.updateProduct(productDto);
            failBecauseExceptionWasNotThrown(NotFoundException.class);
        }
        catch (NotFoundException e) {
            assertThat(e).hasMessage("Product with ID 1 not found.");
        }
    }

    @Test
    public void shouldFailToUpdateProductWhenNameExists() {
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "categoryDto")).subcategoryDto(new SubcategoryDto(1L, "subcategoryDto")).build();

        when(productRepository.existsById(1L)).thenReturn(true);
        when(productRepository.existsByName("productTest")).thenReturn(true);

        try {
            productService.updateProduct(productDto);
            failBecauseExceptionWasNotThrown(ProductCreationException.class);
        }
        catch (ProductCreationException e) {
            assertThat(e).hasMessage("Product with name productTest already exists");
        }
    }

    @Test
    public void shouldDeleteProduct() {
        when(productRepository.existsById(1L)).thenReturn(true);

        productService.deleteProductById(1L);
        verify(productRepository, times(1)).deleteById(1L);
    }

    @Test
    public void shouldFailToDeleteProductWhenIdNotExist() {
        when(productRepository.existsById(1L)).thenReturn(false);

        try {
            productService.deleteProductById(1L);
            failBecauseExceptionWasNotThrown(NotFoundException.class);
        }
        catch (NotFoundException e) {
            assertThat(e).hasMessage("Product with ID 1 not found.");
        }
    }

    @Test
    public void shouldGetMaxProductPrice() {
        Product product = Product.builder().id(1L).name("productTest").description("description").price(new BigDecimal(1))
                .category(new Category(1L, "category")).subcategory(new Subcategory(1L, "subcategory")).build();

        when(productRepository.findFirstByOrderByPriceDesc()).thenReturn(product);

        assertThat(productService.getMaxProductPrice()).isEqualTo(product.getPrice());
    }

}