package hr.pios.cantstop.service;

import hr.pios.cantstop.converter.SubcategoryConverter;
import hr.pios.cantstop.dto.SubcategoryDto;
import hr.pios.cantstop.exception.NotFoundException;
import hr.pios.cantstop.exception.SubcategoryCreationException;
import hr.pios.cantstop.model.Subcategory;
import hr.pios.cantstop.repository.SubcategoryRepository;
import hr.pios.cantstop.service.impl.SubcategoryServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SubcategoryServiceTest {

    @InjectMocks
    private SubcategoryServiceImpl subcategoryService;

    @Mock
    private SubcategoryRepository subcategoryRepository;

    @Mock
    private SubcategoryConverter subcategoryConverter;

    @Test
    public void shouldGetAllSubcategories() {
        Subcategory subcategory = Subcategory.builder().id(1L).name("subcategoryTest").build();
        SubcategoryDto subcategoryDto = SubcategoryDto.builder().id(1L).name("subcategoryTest").build();
        List<Subcategory> categories = List.of(subcategory);
        List<SubcategoryDto> expectedCategories = List.of(subcategoryDto);

        when(subcategoryRepository.findAll()).thenReturn(categories);
        when(subcategoryConverter.convert(subcategory)).thenReturn(subcategoryDto);

        List<SubcategoryDto> subcategoryDtos = subcategoryService.getAllSubcategories();

        assertThat(subcategoryDtos).isEqualTo(expectedCategories);
    }

    @Test
    public void shouldCreateSubcategoryAndReturnDto() {
        Subcategory subcategory = Subcategory.builder().id(1L).name("subcategoryTest").build();
        SubcategoryDto subcategoryDto = SubcategoryDto.builder().id(1L).name("subcategoryTest").build();

        when(subcategoryConverter.convert(subcategory)).thenReturn(subcategoryDto);
        when(subcategoryConverter.convert(subcategoryDto)).thenReturn(subcategory);
        when(subcategoryRepository.save(subcategory)).thenReturn(subcategory);

        SubcategoryDto createDto = subcategoryService.createSubcategory(subcategoryDto);

        assertThat(createDto).isEqualTo(subcategoryDto);
    }

    @Test
    public void shouldFailToCreateSubcategoryWhenNameExists() {
        SubcategoryDto subcategoryDto = SubcategoryDto.builder().id(1L).name("subcategoryTest").build();

        when(subcategoryRepository.existsByName("subcategoryTest")).thenReturn(true);

        try {
            subcategoryService.createSubcategory(subcategoryDto);
            failBecauseExceptionWasNotThrown(SubcategoryCreationException.class);
        }
        catch (SubcategoryCreationException e) {
            assertThat(e).hasMessage("Subcategory with name subcategoryTest already exists");
        }
    }

    @Test
    public void shouldUpdateSubcategoryAndReturnDto() {
        Subcategory subcategory = Subcategory.builder().id(1L).name("subcategoryTest").build();
        SubcategoryDto subcategoryDto = SubcategoryDto.builder().id(1L).name("subcategoryTest").build();

        when(subcategoryConverter.convert(subcategoryDto)).thenReturn(subcategory);
        when(subcategoryConverter.convert(subcategory)).thenReturn(subcategoryDto);
        when(subcategoryRepository.existsById(1L)).thenReturn(true);
        when(subcategoryRepository.existsByName("subcategoryTest")).thenReturn(false);
        when(subcategoryRepository.save(subcategory)).thenReturn(subcategory);

        SubcategoryDto updateDto = subcategoryService.updateSubcategory(subcategoryDto);

        assertThat(updateDto).isEqualTo(subcategoryDto);
    }

    @Test
    public void shouldFailToUpdateSubcategoryWhenIdNotExists() {
        SubcategoryDto subcategoryDto = SubcategoryDto.builder().id(1L).name("subcategoryTest").build();

        when(subcategoryRepository.existsById(1L)).thenReturn(false);

        try {
            subcategoryService.updateSubcategory(subcategoryDto);
            failBecauseExceptionWasNotThrown(NotFoundException.class);
        }
        catch (NotFoundException e) {
            assertThat(e).hasMessage("Category with ID 1 not found.");
        }
    }

    @Test
    public void shouldFailToUpdateSubategoryWhenNameExists() {
        SubcategoryDto subcategoryDto = SubcategoryDto.builder().id(1L).name("subcategoryTest").build();

        when(subcategoryRepository.existsById(1L)).thenReturn(true);
        when(subcategoryRepository.existsByName("subcategoryTest")).thenReturn(true);

        try {
            subcategoryService.updateSubcategory(subcategoryDto);
            failBecauseExceptionWasNotThrown(SubcategoryCreationException.class);
        }
        catch (SubcategoryCreationException e) {
            assertThat(e).hasMessage("Subcategory with name subcategoryTest already exists");
        }
    }

    @Test
    public void shouldDeleteSubcategory() {
        when(subcategoryRepository.existsById(1L)).thenReturn(true);

        subcategoryService.deleteSubcategoryById(1L);
        verify(subcategoryRepository, times(1)).deleteById(1L);
    }

    @Test
    public void shouldFailToDeleteSubcategoryWhenIdNotExist() {
        when(subcategoryRepository.existsById(1L)).thenReturn(false);

        try {
            subcategoryService.deleteSubcategoryById(1L);
            failBecauseExceptionWasNotThrown(NotFoundException.class);
        }
        catch (NotFoundException e) {
            assertThat(e).hasMessage("Category with ID 1 not found.");
        }
    }
}