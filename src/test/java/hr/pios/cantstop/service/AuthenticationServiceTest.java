package hr.pios.cantstop.service;


import hr.pios.cantstop.converter.RoleConverter;
import hr.pios.cantstop.converter.UserConverter;
import hr.pios.cantstop.dto.LoginDto;
import hr.pios.cantstop.dto.RoleDto;
import hr.pios.cantstop.dto.UserDto;
import hr.pios.cantstop.exception.RegistrationException;
import hr.pios.cantstop.jwt.JwtResponse;
import hr.pios.cantstop.jwt.JwtTokenBuilder;
import hr.pios.cantstop.model.Role;
import hr.pios.cantstop.model.User;
import hr.pios.cantstop.repository.RoleRepository;
import hr.pios.cantstop.repository.UserRepository;
import hr.pios.cantstop.service.impl.AuthenticationServiceImpl;
import java.time.LocalDate;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationServiceTest {

    @InjectMocks
    private AuthenticationServiceImpl authenticationService;

    @InjectMocks
    private JwtTokenBuilder jwtTokenBuilder;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private UserConverter userConverter;

    @Mock
    private RoleConverter roleConverter;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private NotificationEmailService notificationEmailService;


    @Test
    public void shouldLoginAUserAndReturnJWT() {
        User user = User.builder().username("admin").password("pass").role(new Role(2L, "ROLE_ADMIN")).build();
        LoginDto loginDto = LoginDto.builder().username("admin").password("pass").build();
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword());
        jwtTokenBuilder.setSecret("kozadnjigazinajmanjekospiceosjeti");

        when(authenticationManager.authenticate(authenticationToken)).thenReturn(authenticationToken);
        when(userRepository.findByUsername(loginDto.getUsername())).thenReturn(user);

        JwtResponse jwtResponse = authenticationService.login(loginDto);

        assertThat(jwtResponse).isNotNull();
    }

    @Test
    public void shouldRegisterUserAndReturnUserDto() {
        User user = User.builder().id(1L).username("admin").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .role(new Role(2L, "ROLE_ADMIN")).build();
        UserDto userDto = UserDto.builder().id(1L).username("admin").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .roleDto(new RoleDto(2L, "ROLE_ADMIN")).build();

        when(userConverter.convert(userDto)).thenReturn(user);
        when(userConverter.convert(user)).thenReturn(userDto);
        when(userRepository.save(user)).thenReturn(user);

        UserDto registerDtoResponse = authenticationService.register(userDto);

        assertThat(registerDtoResponse).isEqualTo(userDto);
    }

    @Test
    public void shouldFailToRegisterUserWhenUsernameAlreadyExists() {
        UserDto userDto = UserDto.builder().id(1L).username("admin").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .roleDto(new RoleDto(2L, "ROLE_ADMIN")).build();

        when(userRepository.existsByUsername(userDto.getUsername())).thenReturn(true);

        try {
            authenticationService.register(userDto);
            failBecauseExceptionWasNotThrown(RegistrationException.class);
        }
        catch (RegistrationException e) {
            assertThat(e).hasMessage("Username already exists. Please use a different username.");
        }
    }

    @Test
    public void shouldFailToRegisterUserWhenEmailAlreadyInUse() {
        UserDto userDto = UserDto.builder().id(1L).username("admin").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .roleDto(new RoleDto(2L, "ROLE_ADMIN")).build();

        when(userRepository.existsByEmail(userDto.getEmail())).thenReturn(true);

        try {
            authenticationService.register(userDto);
            failBecauseExceptionWasNotThrown(RegistrationException.class);
        }
        catch (RegistrationException e) {
            assertThat(e).hasMessage("Email address is already in use. Please use a different address.");
        }
    }

    @Test
    public void shouldGetAllRoles() {
        Role roleUser = new Role(1L, "ROLE_USER");
        Role roleAdmin = new Role(2L, "ROLE_ADMIN");
        List<Role> roles = List.of(roleUser, roleAdmin);
        RoleDto roleUserDto = new RoleDto(1L, "ROLE_USER");
        RoleDto roleAdminDto = new RoleDto(2L, "ROLE_ADMIN");
        List<RoleDto> expectedRoleDtos = List.of(roleUserDto, roleAdminDto);

        when(roleRepository.findAll()).thenReturn(roles);
        when(roleConverter.convert(roleUser)).thenReturn(roleUserDto);
        when(roleConverter.convert(roleAdmin)).thenReturn(roleAdminDto);

        List<RoleDto> responseRoleDtos = authenticationService.getAllRoles();

        assertThat(responseRoleDtos).isEqualTo(expectedRoleDtos);
    }
}