package hr.pios.cantstop.service;

import hr.pios.cantstop.converter.LoyaltyPointsConverter;
import hr.pios.cantstop.dto.LoyaltyPointsDto;
import hr.pios.cantstop.exception.NotEnoughLoyaltyPointsException;
import hr.pios.cantstop.exception.NotFoundException;
import hr.pios.cantstop.model.LoyaltyPoints;
import hr.pios.cantstop.model.Role;
import hr.pios.cantstop.model.User;
import hr.pios.cantstop.repository.LoyaltyPointsRepository;
import hr.pios.cantstop.repository.UserRepository;
import hr.pios.cantstop.service.impl.LoyaltyPointsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoyaltyPointsServiceTest {

    @InjectMocks
    private LoyaltyPointsServiceImpl loyaltyPointsService;

    @Mock
    private LoyaltyPointsRepository loyaltyPointsRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private LoyaltyPointsConverter loyaltyPointsConverter;


    @Test
    public void shouldGetUsersLoyaltyPoints() {
        User user = User.builder().id(1L).username("userTest").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com").role(new Role(2L, "ROLE_ADMIN")).build();
        LoyaltyPoints loyaltyPoints = LoyaltyPoints.builder().id(1L).points(1).user(user).build();
        LoyaltyPointsDto loyaltyPointsDto = LoyaltyPointsDto.builder().userId(1L).loyaltyPoints(1).build();

        when(loyaltyPointsRepository.existsByUser_Id(1L)).thenReturn(true);
        when(loyaltyPointsRepository.findByUser_Id(1L)).thenReturn(loyaltyPoints);
        when(loyaltyPointsConverter.convert(loyaltyPoints)).thenReturn(loyaltyPointsDto);

        LoyaltyPointsDto responseLoyaltyPoints = loyaltyPointsService.getUserLoyaltyPoints(1L);

        assertThat(responseLoyaltyPoints).isEqualTo(loyaltyPointsDto);
    }

    @Test
    public void shouldFailToGetUsersLoyaltyPointsWhenIdNotFound() {
        when(loyaltyPointsRepository.existsByUser_Id(1L)).thenReturn(false);

        try {
            loyaltyPointsService.getUserLoyaltyPoints(1L);
            failBecauseExceptionWasNotThrown(NotFoundException.class);
        }
        catch (NotFoundException e) {
            assertThat(e).hasMessage("Loyalty points for user with ID 1 not found.");
        }
    }

    @Test
    public void shouldIncreaseUsersLoyaltyPoints() {
        User user = User.builder().id(1L).username("userTest").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .role(new Role(2L, "ROLE_ADMIN")).build();
        LoyaltyPoints loyaltyPoints = LoyaltyPoints.builder().id(1L).points(1).user(user).build();

        when(loyaltyPointsRepository.existsByUser_Id(1L)).thenReturn(true);
        when(loyaltyPointsRepository.findByUser_Id(1L)).thenReturn(loyaltyPoints);

        loyaltyPointsService.increaseLoyaltyPoints(1L, 1);

        assertThat(loyaltyPoints.getPoints()).isEqualTo(2);
    }

    @Test
    public void shouldFailToIncreaseUsersLoyaltyPointsWhenUserIdNotFound() {
        when(loyaltyPointsRepository.existsByUser_Id(1L)).thenReturn(false);

        try {
            loyaltyPointsService.increaseLoyaltyPoints(1L, 1);
            failBecauseExceptionWasNotThrown(NotFoundException.class);
        }
        catch (NotFoundException e) {
            assertThat(e).hasMessage("User with ID 1 not found.");
        }
    }

    @Test
    public void shouldDecreaseUsersLoyaltyPoints() {
        User user = User.builder().id(1L).username("userTest").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .role(new Role(2L, "ROLE_ADMIN")).build();
        LoyaltyPoints loyaltyPoints = LoyaltyPoints.builder().id(1L).points(2).user(user).build();

        when(loyaltyPointsRepository.existsByUser_Id(1L)).thenReturn(true);
        when(loyaltyPointsRepository.findByUser_Id(1L)).thenReturn(loyaltyPoints);

        loyaltyPointsService.decreaseLoyaltyPoints(1L, 1);

        assertThat(loyaltyPoints.getPoints()).isEqualTo(1);
    }

    @Test
    public void shouldFailToDecreaseUsersLoyaltyPointsWhenUserDoesntHaveEnoughLoyaltyPoints() {
        User user = User.builder().id(1L).username("userTest").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .role(new Role(2L, "ROLE_ADMIN")).build();
        LoyaltyPoints loyaltyPoints = LoyaltyPoints.builder().id(1L).points(0).user(user).build();

        when(loyaltyPointsRepository.existsByUser_Id(1L)).thenReturn(true);
        when(loyaltyPointsRepository.findByUser_Id(1L)).thenReturn(loyaltyPoints);

        try {
            loyaltyPointsService.decreaseLoyaltyPoints(1L, 1);
            failBecauseExceptionWasNotThrown(NotEnoughLoyaltyPointsException.class);
        }
        catch (NotEnoughLoyaltyPointsException e) {
            assertThat(e).hasMessage("User with ID 1 does not have enough loyalty points");
        }
    }

    @Test
    public void shouldReturnTrueWhenLoyaltyPointsExistsByUserId() {
        when(loyaltyPointsRepository.existsByUser_Id(1L)).thenReturn(true);

        Boolean expectedResponse = loyaltyPointsService.existsByUserId(1L);

        assertThat(expectedResponse).isEqualTo(true);
    }

    @Test
    public void shouldReturnFalseWhenLoyaltyPointsNotExistsByUserId() {
        when(loyaltyPointsRepository.existsByUser_Id(1L)).thenReturn(false);

        Boolean expectedResponse = loyaltyPointsService.existsByUserId(1L);

        assertThat(expectedResponse).isEqualTo(false);
    }

    @Test
    public void shouldReturnTrueWhenUserHasEnoughLoyaltyPoints() {
        User user = User.builder().id(1L).username("userTest").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .role(new Role(2L, "ROLE_ADMIN")).build();
        LoyaltyPoints loyaltyPoints = LoyaltyPoints.builder().id(1L).points(5).user(user).build();


        when(loyaltyPointsRepository.findByUser_Id(1L)).thenReturn(loyaltyPoints);
        when(loyaltyPointsRepository.existsByUser_Id(1L)).thenReturn(true);
        Boolean expectedResponse = loyaltyPointsService.hasEnoughLoyaltyPoints(1L, 2);

        assertThat(expectedResponse).isEqualTo(true);
    }

    @Test
    public void shouldReturnFalseWhenUserHasNotEnoughLoyaltyPoints() {
        User user = User.builder().id(1L).username("userTest").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .role(new Role(2L, "ROLE_ADMIN")).build();
        LoyaltyPoints loyaltyPoints = LoyaltyPoints.builder().id(1L).points(1).user(user).build();

        when(loyaltyPointsRepository.existsByUser_Id(1L)).thenReturn(true);
        when(loyaltyPointsRepository.findByUser_Id(1L)).thenReturn(loyaltyPoints);
        Boolean expectedResponse = loyaltyPointsService.hasEnoughLoyaltyPoints(1L, 5);

        assertThat(expectedResponse).isEqualTo(false);
    }
}
