package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.OrderDto;
import hr.pios.cantstop.dto.RoleDto;
import hr.pios.cantstop.dto.UserDto;
import hr.pios.cantstop.model.Order;
import hr.pios.cantstop.model.Role;
import hr.pios.cantstop.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class OrderConverterTest {

    @InjectMocks
    private OrderConverter orderConverter;

    @Mock
    private UserConverter userConverter;

    @Test
    public void shouldConvertOrderToOrderDto() {
        UserDto userDto = UserDto.builder().id(1L).username("admin").password("pass").firstName("Admin")
                .lastName("Admin").dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("admin.admin@email.com").roleDto(new RoleDto(2L, "ROLE_ADMIN")).build();
        User user = User.builder().id(1L).username("admin").password("pass").firstName("Admin")
                .lastName("Admin").dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("admin.admin@email.com").role(new Role(2L, "ROLE_ADMIN")).build();
        Order order = Order.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1)
                .totalPrice(new BigDecimal(1)).date(LocalDate.of(2020, 2, 2))
                .user(user).build();
        OrderDto orderDto = OrderDto.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1)
                .totalPrice(new BigDecimal(1)).date(LocalDate.of(2020, 2, 2))
                .userDto(userDto).build();

        when(userConverter.convert(user)).thenReturn(userDto);

        assertThat(orderConverter.convert(order)).isEqualTo(orderDto);
    }

    @Test
    public void shouldConvertOrderDtoToOrder() {
        UserDto userDto = UserDto.builder().id(1L).username("admin").password("pass").firstName("Admin")
                .lastName("Admin").dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("admin.admin@email.com").roleDto(new RoleDto(2L, "ROLE_ADMIN")).build();
        User user = User.builder().id(1L).username("admin").password("pass").firstName("Admin")
                .lastName("Admin").dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("admin.admin@email.com").role(new Role(2L, "ROLE_ADMIN")).build();
        OrderDto orderDto = OrderDto.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1)
                .totalPrice(new BigDecimal(1)).date(LocalDate.of(2020, 2, 2))
                .userDto(userDto).build();
        Order order = Order.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1)
                .totalPrice(new BigDecimal(1)).date(LocalDate.of(2020, 2, 2))
                .user(user).build();

        when(userConverter.convert(userDto)).thenReturn(user);

        assertThat(orderConverter.convert(orderDto)).isEqualTo(order);
    }
}
