package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.RoleDto;
import hr.pios.cantstop.dto.UserDto;
import hr.pios.cantstop.model.Role;
import hr.pios.cantstop.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class UserConverterTest {

    @InjectMocks
    private UserConverter userConverter;

    @Mock
    private RoleConverter roleConverter;

    @Test
    public void shouldConvertUserToUserDto() {
        User user = User.builder()
                .id(1L)
                .username("admin")
                .password("pass")
                .firstName("Admin")
                .lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("admin.admin@email.com")
                .role(roleConverter.convert(new RoleDto(2L, "ROLE_ADMIN")))
                .build();
        UserDto ExpectedDto = UserDto.builder()
                .id(1L)
                .username("admin")
                .password("pass")
                .firstName("Admin")
                .lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("admin.admin@email.com")
                .roleDto(roleConverter.convert(new Role(2L, "ROLE_ADMIN")))
                .build();

        assertThat(userConverter.convert(user)).isEqualTo(ExpectedDto);
    }

    @Test
    public void shouldConvertUserDtoToUser() {
        UserDto userDto = UserDto.builder()
                .id(1L)
                .username("admin")
                .password("pass")
                .firstName("Admin")
                .lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("admin.admin@email.com")
                .roleDto(roleConverter.convert(new Role(2L, "ROLE_ADMIN")))
                .build();
        User expectedUser = User.builder()
                .id(1L)
                .username("admin")
                .password("pass")
                .firstName("Admin")
                .lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("admin.admin@email.com")
                .role(roleConverter.convert(new RoleDto(2L, "ROLE_ADMIN")))
                .build();


        assertThat(userConverter.convert(userDto)).isEqualTo(expectedUser);
    }
}
