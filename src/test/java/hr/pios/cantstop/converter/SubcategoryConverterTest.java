package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.SubcategoryDto;
import hr.pios.cantstop.model.Subcategory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class SubcategoryConverterTest {

    @InjectMocks
    private SubcategoryConverter subcategoryConverter;


    @Test
    public void shouldConvertSubcategoryToSubcategoryDto() {
        Subcategory subcategory = Subcategory.builder().id(1L).name("subcategory").build();
        SubcategoryDto expectedDto = SubcategoryDto.builder().id(1L).name("subcategory").build();

        assertThat(subcategoryConverter.convert(subcategory)).isEqualTo(expectedDto);
    }

    @Test
    public void shouldConvertSubcategoryDtoToSubcategory() {
        SubcategoryDto categoryDto = SubcategoryDto.builder().id(1L).name("subcategory").build();
        Subcategory expectedSubcategory = Subcategory.builder().id(1L).name("subcategory").build();

        assertThat(subcategoryConverter.convert(categoryDto)).isEqualTo(expectedSubcategory);
    }
}
