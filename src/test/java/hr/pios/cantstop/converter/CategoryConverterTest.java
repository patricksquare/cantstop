package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.CategoryDto;
import hr.pios.cantstop.model.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CategoryConverterTest {

    @InjectMocks
    private CategoryConverter categoryConverter;


    @Test
    public void shouldConvertCategoryToCategoryDto() {
        Category category = Category.builder().id(1L).name("category").build();
        CategoryDto expectedDto = CategoryDto.builder().id(1L).name("category").build();

        assertThat(categoryConverter.convert(category)).isEqualTo(expectedDto);
    }

    @Test
    public void shouldConvertCategoryDtoToCategory() {
        CategoryDto categoryDto = CategoryDto.builder().id(1L).name("category").build();
        Category expectedCategory = Category.builder().id(1L).name("category").build();

        assertThat(categoryConverter.convert(categoryDto)).isEqualTo(expectedCategory);
    }
}
