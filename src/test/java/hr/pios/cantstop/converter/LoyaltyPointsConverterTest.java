package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.LoyaltyPointsDto;
import hr.pios.cantstop.model.LoyaltyPoints;
import hr.pios.cantstop.model.Role;
import hr.pios.cantstop.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class LoyaltyPointsConverterTest {

    @InjectMocks
    private LoyaltyPointsConverter loyaltyPointsConverter;


    @Test
    public void shouldConvertLoyaltyPointsToLoyaltyPointsDto() {
        User user = User.builder().id(1L).username("userTest").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .role(new Role(2L, "ROLE_ADMIN")).build();
        LoyaltyPoints loyaltyPoints = LoyaltyPoints.builder().id(1L).points(1).user(user).build();
        LoyaltyPointsDto loyaltyPointsDto = LoyaltyPointsDto.builder().loyaltyPoints(1).userId(1L).build();

        assertThat(loyaltyPointsConverter.convert(loyaltyPoints)).isEqualTo(loyaltyPointsDto);
    }
}
