package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.*;
import hr.pios.cantstop.model.*;
import hr.pios.cantstop.repository.OrderProductRepository;
import hr.pios.cantstop.repository.OrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderProductConverterTest {

    @InjectMocks
    private OrderProductConverter orderProductConverter;

    @Mock
    private OrderConverter orderConverter;

    @Mock
    private ProductConverter productConverter;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private OrderProductRepository orderProductRepository;


    @Test
    public void shouldConvertOrderProductToOrderProductDto() {
        UserDto userDto = UserDto.builder().id(1L).username("admin").password("pass").firstName("Admin")
                .lastName("Admin").dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("admin.admin@email.com").roleDto(new RoleDto(2L, "ROLE_ADMIN")).build();
        User user = User.builder().id(1L).username("admin").password("pass").firstName("Admin")
                .lastName("Admin").dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("admin.admin@email.com").role(new Role(2L, "ROLE_ADMIN")).build();
        Order order = Order.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1)
                .totalPrice(new BigDecimal(1)).date(LocalDate.of(2020, 2, 2)).user(user).build();
        OrderDto orderDto = OrderDto.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1)
                .totalPrice(new BigDecimal(1)).date(LocalDate.of(2020, 2, 2))
                .userDto(userDto).build();
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description")
                .price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "categoryDto"))
                .subcategoryDto(new SubcategoryDto(1L, "subcategoryDto")).build();
        Product product = Product.builder().id(1L).name("productTest").description("description")
                .price(new BigDecimal(1)).category(new Category(1L, "categoryDto"))
                .subcategory(new Subcategory(1L, "subcategoryDto")).build();
        OrderProductDto orderProductDto = OrderProductDto.builder().productMap(Map.of(productDto, 1))
                .orderDto(orderDto).build();
        OrderProductId orderProductId = OrderProductId.builder().order(order).product(product).build();
        OrderProduct orderProduct = OrderProduct.builder().orderProductId(orderProductId).quantity(1).build();
        List<OrderProduct> orderProducts = List.of(orderProduct);

        when(orderConverter.convert(order)).thenReturn(orderDto);
        when(productConverter.convert(product)).thenReturn(productDto);
        when(orderRepository.findById(1L)).thenReturn(Optional.of(order));
        when(orderProductRepository.findAllByOrderProductId_Order_Id(1L)).thenReturn(orderProducts);

        assertThat(orderProductConverter.convert(orderProduct)).isEqualTo(orderProductDto);
    }
}
