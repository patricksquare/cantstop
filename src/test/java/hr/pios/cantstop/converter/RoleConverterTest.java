package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.RoleDto;
import hr.pios.cantstop.model.Role;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class RoleConverterTest {

    @InjectMocks
    private RoleConverter roleConverter;


    @Test
    public void shouldConvertRoleToRoleDto() {
        Role role = Role.builder().id(2L).name("ROLE_ADMIN").build();
        RoleDto expectedDto = RoleDto.builder().id(2L).name("ROLE_ADMIN").build();

        assertThat(roleConverter.convert(role)).isEqualTo(expectedDto);
    }

    @Test
    public void shouldConvertRoleDtoToRole() {
        RoleDto categoryDto = RoleDto.builder().id(2L).name("ROLE_ADMIN").build();
        Role expectedRole = Role.builder().id(2L).name("ROLE_ADMIN").build();

        assertThat(roleConverter.convert(categoryDto)).isEqualTo(expectedRole);
    }
}
