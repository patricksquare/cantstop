package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.CategoryDto;
import hr.pios.cantstop.dto.ProductDto;
import hr.pios.cantstop.dto.SubcategoryDto;
import hr.pios.cantstop.model.Category;
import hr.pios.cantstop.model.Product;
import hr.pios.cantstop.model.Subcategory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import java.math.BigDecimal;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ProductConverterTest {

    @InjectMocks
    private ProductConverter productConverter;

    @Mock
    private CategoryConverter categoryConverter;

    @Mock
    private SubcategoryConverter subcategoryConverter;


    @Test
    public void shouldConvertProductToProductDto() {
        Product product = Product.builder()
                .id(1L).name("product")
                .description("description")
                .price(new BigDecimal(2))
                .category(categoryConverter.convert(new CategoryDto(1L, "category")))
                .subcategory(subcategoryConverter.convert(new SubcategoryDto(1L, "subcategory")))
                .build();
        ProductDto expectedDto = ProductDto.builder()
                .id(1L).name("product")
                .description("description")
                .price(new BigDecimal(2))
                .categoryDto(categoryConverter.convert(new Category(1L, "category")))
                .subcategoryDto(subcategoryConverter.convert(new Subcategory(1L, "subcategory")))
                .build();

        assertThat(productConverter.convert(product)).isEqualTo(expectedDto);
    }

    @Test
    public void shouldConvertProductDtoToProduct() {
        ProductDto productDto = ProductDto.builder()
                .id(1L).name("product")
                .description("description")
                .price(new BigDecimal(2))
                .categoryDto(categoryConverter.convert(new Category(1L, "category")))
                .subcategoryDto(subcategoryConverter.convert(new Subcategory(1L, "subcategory")))
                .build();
        Product expectedProduct = Product.builder()
                .id(1L).name("product")
                .description("description")
                .price(new BigDecimal(2))
                .category(categoryConverter.convert(new CategoryDto(1L, "category")))
                .subcategory(subcategoryConverter.convert(new SubcategoryDto(1L, "subcategory")))
                .build();

        assertThat(productConverter.convert(productDto)).isEqualTo(expectedProduct);
    }
}

