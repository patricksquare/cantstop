package hr.pios.cantstop.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import hr.pios.cantstop.advice.ExceptionAdvice;
import hr.pios.cantstop.dto.ApiResponse;
import hr.pios.cantstop.dto.LoginDto;
import hr.pios.cantstop.dto.RoleDto;
import hr.pios.cantstop.dto.UserDto;
import hr.pios.cantstop.jwt.JwtResponse;
import hr.pios.cantstop.service.AuthenticationService;
import hr.pios.cantstop.support.CantStopMvcTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@CantStopMvcTest(AuthenticationController.class)
@ImportAutoConfiguration(ExceptionAdvice.class)
public class AuthenticationControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AuthenticationService authenticationService;


    @Test
    public void shouldLoginAUserAndReturnToken() throws Exception {
        LoginDto loginDto = new LoginDto("admin", "pass");
        JwtResponse jwtResponse = new JwtResponse("token");

        when(authenticationService.login(loginDto)).thenReturn(jwtResponse);

        mvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(loginDto)))
                .andExpect(status().isOk())
                .andExpect(content().string(asJsonString(new ApiResponse(jwtResponse))))
                .andDo(print());
    }

    @Test
    public void shouldRegisterAUserAndReturnRegisterDto() throws Exception {
        UserDto userDto = new UserDto("admin", "pass", new RoleDto(2L, "ROLE_ADMIN"));

        when(authenticationService.register(userDto)).thenReturn(userDto);

        mvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(userDto)))
                .andExpect(status().isCreated())
                .andExpect(content().string(asJsonString(new ApiResponse(userDto))))
                .andDo(print());
    }

    @Test
    public void shouldGetAllRoles() throws Exception {
        List<RoleDto> expectedRoleDtos = List.of(new RoleDto(1L, "ROLE_USER"), new RoleDto(2L, "ROLE_ADMIN"));

        mvc.perform(get("/api/auth/roles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(expectedRoleDtos)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    private String asJsonString(final Object obj) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            return objectMapper.writeValueAsString(obj);
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}