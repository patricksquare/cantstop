package hr.pios.cantstop.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import hr.pios.cantstop.advice.ExceptionAdvice;
import hr.pios.cantstop.dto.*;
import hr.pios.cantstop.model.Role;
import hr.pios.cantstop.model.User;
import hr.pios.cantstop.service.CurrentUserService;
import hr.pios.cantstop.service.OrderService;
import hr.pios.cantstop.support.CantStopMvcTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@CantStopMvcTest(OrderController.class)
@ImportAutoConfiguration(ExceptionAdvice.class)
public class OrderControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private OrderService orderService;

    @MockBean
    CurrentUserService currentUserService;

    @Test
    public void shouldGetAllOrders() throws Exception {
        UserDto userDto = UserDto.builder().id(1L).username("admin").password("pass").firstName("Admin")
                .lastName("Admin").dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("admin.admin@email.com").roleDto(new RoleDto(2L, "ROLE_ADMIN")).build();
        OrderDto orderDto = OrderDto.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1)
                .totalPrice(new BigDecimal(1)).date(LocalDate.of(2000, 2, 1))
                .userDto(userDto).build();
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description")
                .price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "categoryDto"))
                .subcategoryDto(new SubcategoryDto(1L, "subcategoryDto")).build();
        OrderProductDto orderProductDto = OrderProductDto.builder().orderDto(orderDto)
                .productMap(Map.of(productDto, 1)).build();

        Page<OrderProductDto> expectedOrders = new PageImpl<>(List.of(orderProductDto));
        FilterOrderDto filter = new FilterOrderDto();

        when(orderService.getAllOrders(0, 1, filter)).thenReturn(expectedOrders);

        mvc.perform(post("/api/order/all-orders?page=0&size=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(filter)))
                .andExpect(status().isOk())
                .andExpect(content().string(asJsonString(new ApiResponse(expectedOrders))))
                .andDo(print());
    }

    @Test
    public void shouldGetAllOrdersFromCurrentUser() throws Exception {
        UserDto userDto = UserDto.builder().id(1L).username("admin").password("pass").firstName("Admin")
                .lastName("Admin").dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("admin.admin@email.com").roleDto(new RoleDto(2L, "ROLE_ADMIN")).build();
        User user = User.builder().id(1L).username("admin").password("pass").firstName("Admin").lastName("Admin")
                .dateOfBirth(LocalDate.of(1998, 1, 1)).email("admin.admin@email.com")
                .role(new Role(2L, "ROLE_ADMIN")).build();
        OrderDto orderDto = OrderDto.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1)
                .totalPrice(new BigDecimal(1)).date(LocalDate.of(2020, 2, 2))
                .userDto(userDto).build();
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description")
                .price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "categoryDto"))
                .subcategoryDto(new SubcategoryDto(1L, "subcategoryDto")).build();
        OrderProductDto orderProductDto = OrderProductDto.builder().orderDto(orderDto)
                .productMap(Map.of(productDto, 0)).build();

        Page<OrderProductDto> expectedOrders = new PageImpl<>(List.of(orderProductDto));
        FilterOrderDto filter = new FilterOrderDto();

        when(orderService.getAllCurrentUserOrders(0, 1, filter)).thenReturn(expectedOrders);
        when(currentUserService.getLoggedInUser()).thenReturn(user);

        mvc.perform(post("/api/order/current-user/all-orders?page=0&size=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(filter)))
                .andExpect(status().isOk())
                .andExpect(content().string(asJsonString(new ApiResponse(expectedOrders))))
                .andDo(print());
    }

    @Test
    public void shouldCreateOrder() throws Exception {
        UserDto userDto = UserDto.builder().id(1L).username("admin").password("pass").firstName("Admin")
                .lastName("Admin").dateOfBirth(LocalDate.of(1998, 1, 1))
                .email("admin.admin@email.com").roleDto(new RoleDto(2L, "ROLE_ADMIN")).build();
        OrderDto orderDto = OrderDto.builder().id(1L).totalPrice(new BigDecimal(1)).discount(1)
                .totalPrice(new BigDecimal(1)).date(LocalDate.of(2000, 2, 1))
                .userDto(userDto).build();
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description")
                .price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "categoryDto"))
                .subcategoryDto(new SubcategoryDto(1L, "subcategoryDto")).build();
        OrderProductDto orderProductDto = OrderProductDto.builder().orderDto(orderDto)
                .productMap(Map.of(productDto, 1)).build();
        CreateOrderProductDto createOrderProductDto = CreateOrderProductDto.builder().productMap(Map.of(1L, 1))
                .discount(1).userId(1L).build();

        List<OrderProductDto> orders= List.of(orderProductDto);

        when(orderService.createOrder(createOrderProductDto)).thenReturn(orders);

        mvc.perform(post("/api/order")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(createOrderProductDto)))
                .andExpect(status().isCreated())
                .andExpect(content().string(asJsonString(new ApiResponse(orders))))
                .andDo(print());
    }

    @Test
    public void shouldDeleteOrder() throws Exception {
        mvc.perform(delete("/api/order/{orderId}", 1L))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    private String asJsonString(final Object obj) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            return objectMapper.writeValueAsString(obj);
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
