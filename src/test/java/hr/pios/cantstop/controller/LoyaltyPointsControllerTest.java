package hr.pios.cantstop.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import hr.pios.cantstop.advice.ExceptionAdvice;
import hr.pios.cantstop.dto.ApiResponse;
import hr.pios.cantstop.dto.LoyaltyPointsDto;
import hr.pios.cantstop.service.LoyaltyPointsService;
import hr.pios.cantstop.support.CantStopMvcTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@CantStopMvcTest(LoyaltyPointsController.class)
@ImportAutoConfiguration(ExceptionAdvice.class)
public class LoyaltyPointsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LoyaltyPointsService loyaltyPointsService;


    @Test
    public void shouldGetUsersLoyaltyPoints() throws Exception {
        LoyaltyPointsDto loyaltyPointsDto = LoyaltyPointsDto.builder().userId(1L).loyaltyPoints(1).build();

        when(loyaltyPointsService.getUserLoyaltyPoints(1L)).thenReturn(loyaltyPointsDto);

        mvc.perform(get("/api/loyalty-points/{userId}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().string(asJsonString(new ApiResponse(loyaltyPointsDto))))
                .andDo(print());
    }

    private String asJsonString(final Object obj) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            return objectMapper.writeValueAsString(obj);
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
