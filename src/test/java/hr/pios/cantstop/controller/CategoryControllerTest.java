package hr.pios.cantstop.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import hr.pios.cantstop.advice.ExceptionAdvice;
import hr.pios.cantstop.dto.ApiResponse;
import hr.pios.cantstop.dto.CategoryDto;
import hr.pios.cantstop.exception.NotFoundException;
import hr.pios.cantstop.service.CategoryService;
import hr.pios.cantstop.support.CantStopMvcTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@CantStopMvcTest(CategoryController.class)
@ImportAutoConfiguration(ExceptionAdvice.class)
public class CategoryControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CategoryService categoryService;


    @Test
    public void shouldGetAllCategories() throws Exception{
        CategoryDto categoryDto = CategoryDto.builder().id(1L).name("categoryTest").build();
        List<CategoryDto> expectedCategories = List.of(categoryDto);

        when(categoryService.getAllCategories()).thenReturn(expectedCategories);

        mvc.perform(get("/api/category"))
                .andExpect(status().isOk())
                .andExpect(content().string(asJsonString(new ApiResponse(expectedCategories))))
                .andDo(print());
    }

    @Test
    public void shouldCreateCategory() throws Exception {
        CategoryDto categoryDto = CategoryDto.builder().id(1L).name("categoryTest").build();

        when(categoryService.createCategory(categoryDto)).thenReturn(categoryDto);

        mvc.perform(post("/api/category")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(categoryDto)))
                .andExpect(status().isCreated())
                .andExpect(content().string(asJsonString(new ApiResponse(categoryDto))))
                .andDo(print());
    }

    @Test
    public void shouldUpdateCategory() throws Exception {
        CategoryDto categoryDto = CategoryDto.builder().id(1L).name("categoryTest").build();

        when(categoryService.updateCategory(categoryDto)).thenReturn(categoryDto);

        mvc.perform(put("/api/category")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(categoryDto)))
                .andExpect(status().isOk())
                .andExpect(content().string(asJsonString(new ApiResponse(categoryDto))))
                .andDo(print());
    }

    @Test
    public void shouldDeleteCategory() throws Exception {
        mvc.perform(delete("/api/category/{categoryId}", 1L))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    private String asJsonString(final Object obj) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            return objectMapper.writeValueAsString(obj);
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
