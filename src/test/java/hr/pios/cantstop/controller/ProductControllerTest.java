package hr.pios.cantstop.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import hr.pios.cantstop.advice.ExceptionAdvice;
import hr.pios.cantstop.dto.*;
import hr.pios.cantstop.service.ProductService;
import hr.pios.cantstop.support.CantStopMvcTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@CantStopMvcTest(ProductController.class)
@ImportAutoConfiguration(ExceptionAdvice.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService productService;


    @Test
    public void shouldGetAllProducts() throws Exception{
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description")
                .price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "categoryDto"))
                .subcategoryDto(new SubcategoryDto(1L, "subcategoryDto")).build();

        Page<ProductDto> expectedProducts = new PageImpl<>(List.of(productDto));
        FilterProductDto filter = new FilterProductDto();

        when(productService.getAllProducts(0, 1, filter)).thenReturn(expectedProducts);

        mvc.perform(post("/api/product/all-products?page=0&size=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(filter)))
                .andExpect(status().isOk())
                .andExpect(content().string(asJsonString(new ApiResponse(expectedProducts))))
                .andDo(print());
    }

    @Test
    public void shouldCreateProduct() throws Exception {
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description")
                .price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "categoryDto"))
                .subcategoryDto(new SubcategoryDto(1L, "subcategoryDto")).build();

        when(productService.createProduct(productDto)).thenReturn(productDto);

        mvc.perform(post("/api/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(productDto)))
                .andExpect(status().isCreated())
                .andExpect(content().string(asJsonString(new ApiResponse(productDto))))
                .andDo(print());
    }

    @Test
    public void shouldUpdateProduct() throws Exception {
        ProductDto productDto = ProductDto.builder().id(1L).name("productTest").description("description")
                .price(new BigDecimal(1)).categoryDto(new CategoryDto(1L, "categoryDto"))
                .subcategoryDto(new SubcategoryDto(1L, "subcategoryDto")).build();

        when(productService.updateProduct(productDto)).thenReturn(productDto);

        mvc.perform(put("/api/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(productDto)))
                .andExpect(status().isOk())
                .andExpect(content().string(asJsonString(new ApiResponse(productDto))))
                .andDo(print());
    }

    @Test
    public void shouldDeleteProduct() throws Exception {
        mvc.perform(delete("/api/product/{productId}", 1L))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    @Test
    public void shouldGetMaxProductPrice() throws Exception {
        BigDecimal maxProductPrice = new BigDecimal(10);

        when(productService.getMaxProductPrice()).thenReturn(maxProductPrice);

        mvc.perform(get("/api/product/max-price"))
                .andExpect(status().isOk())
                .andExpect(content().string(asJsonString(new ApiResponse(maxProductPrice))))
                .andDo(print());
    }

    private String asJsonString(final Object obj) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            return objectMapper.writeValueAsString(obj);
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
