package hr.pios.cantstop.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import hr.pios.cantstop.advice.ExceptionAdvice;
import hr.pios.cantstop.dto.ApiResponse;
import hr.pios.cantstop.dto.SubcategoryDto;
import hr.pios.cantstop.service.SubcategoryService;
import hr.pios.cantstop.support.CantStopMvcTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@CantStopMvcTest(SubcategoryController.class)
@ImportAutoConfiguration(ExceptionAdvice.class)
public class SubcategoryControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SubcategoryService subcategoryService;


    @Test
    public void shouldGetAllSubcategories() throws Exception{
        SubcategoryDto subcategoryDto = SubcategoryDto.builder().id(1L).name("subcategoryTest").build();
        List<SubcategoryDto> expectedSubcategories = List.of(subcategoryDto);

        when(subcategoryService.getAllSubcategories()).thenReturn(expectedSubcategories);

        mvc.perform(get("/api/subcategory"))
                .andExpect(status().isOk())
                .andExpect(content().string(asJsonString(new ApiResponse(expectedSubcategories))))
                .andDo(print());
    }

    @Test
    public void shouldCreateSubcategory() throws Exception {
        SubcategoryDto subcategoryDto = SubcategoryDto.builder().id(1L).name("subcategoryTest").build();

        when(subcategoryService.createSubcategory(subcategoryDto)).thenReturn(subcategoryDto);

        mvc.perform(post("/api/subcategory")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(subcategoryDto)))
                .andExpect(status().isCreated())
                .andExpect(content().string(asJsonString(new ApiResponse(subcategoryDto))))
                .andDo(print());
    }

    @Test
    public void shouldUpdateSubcategory() throws Exception {
        SubcategoryDto subcategoryDto = SubcategoryDto.builder().id(1L).name("subcategoryTest").build();

        when(subcategoryService.updateSubcategory(subcategoryDto)).thenReturn(subcategoryDto);

        mvc.perform(put("/api/subcategory")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(subcategoryDto)))
                .andExpect(status().isOk())
                .andExpect(content().string(asJsonString(new ApiResponse(subcategoryDto))))
                .andDo(print());
    }

    @Test
    public void shouldDeleteSubcategory() throws Exception {
        mvc.perform(delete("/api/subcategory/{categoryId}", 1L))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    private String asJsonString(final Object obj) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            return objectMapper.writeValueAsString(obj);
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
