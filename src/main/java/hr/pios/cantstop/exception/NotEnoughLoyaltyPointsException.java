package hr.pios.cantstop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NotEnoughLoyaltyPointsException extends RuntimeException {

    public NotEnoughLoyaltyPointsException(final String message) {
        super(message);
    }

    public NotEnoughLoyaltyPointsException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public NotEnoughLoyaltyPointsException(final Throwable cause) {
        super(cause);
    }
}
