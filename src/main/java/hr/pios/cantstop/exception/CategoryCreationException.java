package hr.pios.cantstop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CategoryCreationException extends RuntimeException {

    public CategoryCreationException(final String message) {
        super(message);
    }

    public CategoryCreationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CategoryCreationException(final Throwable cause) {
        super(cause);
    }
}