package hr.pios.cantstop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ProductCreationException extends RuntimeException {

    public ProductCreationException(final String message) {
        super(message);
    }

    public ProductCreationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ProductCreationException(final Throwable cause) {
        super(cause);
    }
}
