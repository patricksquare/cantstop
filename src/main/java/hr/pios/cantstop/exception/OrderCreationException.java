package hr.pios.cantstop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class OrderCreationException extends RuntimeException {

    public OrderCreationException(final String message) {
        super(message);
    }

    public OrderCreationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public OrderCreationException(final Throwable cause) {
        super(cause);
    }
}
