package hr.pios.cantstop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class SubcategoryCreationException extends RuntimeException {

    public SubcategoryCreationException(final String message) {
        super(message);
    }

    public SubcategoryCreationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public SubcategoryCreationException(final Throwable cause) {
        super(cause);
    }
}