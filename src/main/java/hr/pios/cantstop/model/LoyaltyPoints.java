package hr.pios.cantstop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "loyalty_points")
public class LoyaltyPoints {

    @Id
    @SequenceGenerator(name = "loyalty_points_id_seq", allocationSize = 1)
    @GeneratedValue(generator = "loyalty_points_id_seq", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "points")
    private Integer points;

    @OneToOne
    private User user;
}
