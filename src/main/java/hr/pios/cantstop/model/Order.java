package hr.pios.cantstop.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "customer_order")
public class Order {

    @Id
    @SequenceGenerator(name = "customer_order_id_seq", allocationSize = 1)
    @GeneratedValue(generator = "customer_order_id_seq", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "total_price")
    private BigDecimal totalPrice;

    @Column(name = "discount")
    private Integer discount;

    @Column(name = "final_price")
    private BigDecimal finalPrice;

    @Column(name = "date")
    private LocalDate date;

    @ManyToOne
    private User user;
}
