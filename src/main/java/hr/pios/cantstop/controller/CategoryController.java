package hr.pios.cantstop.controller;

import hr.pios.cantstop.dto.ApiResponse;
import hr.pios.cantstop.dto.CategoryDto;
import hr.pios.cantstop.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/category")
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class CategoryController {
    
    private final CategoryService categoryService;

    @GetMapping
    public ResponseEntity<ApiResponse> getAllCategories() {
        return new ResponseEntity<>(new ApiResponse(categoryService.getAllCategories()), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ApiResponse> createCategory(@RequestBody final CategoryDto categoryDto) {
        return new ResponseEntity<>(new ApiResponse(categoryService.createCategory(categoryDto)), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<ApiResponse> updateCategory(@RequestBody final CategoryDto categoryDto) {
        return new ResponseEntity<>(new ApiResponse(categoryService.updateCategory(categoryDto)), HttpStatus.OK);
    }

    @DeleteMapping("/{categoryId}")
    public ResponseEntity<ApiResponse> deleteCategory(@PathVariable final Long categoryId) {
        categoryService.deleteCategoryById(categoryId);
        return ResponseEntity.noContent().build();
    }
}
