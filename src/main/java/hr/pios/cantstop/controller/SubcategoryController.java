package hr.pios.cantstop.controller;

import hr.pios.cantstop.dto.ApiResponse;
import hr.pios.cantstop.dto.SubcategoryDto;
import hr.pios.cantstop.service.SubcategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/subcategory")
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class SubcategoryController {

    private final SubcategoryService subcategoryService;

    @GetMapping
    public ResponseEntity<ApiResponse> getAllSubcategories() {
        return new ResponseEntity<>(new ApiResponse(subcategoryService.getAllSubcategories()), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ApiResponse> createSubSubCategory(@RequestBody final SubcategoryDto subcategoryDto) {
        return new ResponseEntity<>(new ApiResponse(subcategoryService.createSubcategory(subcategoryDto)), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<ApiResponse> updateSubCategory(@RequestBody final SubcategoryDto subcategoryDto) {
        return new ResponseEntity<>(new ApiResponse(subcategoryService.updateSubcategory(subcategoryDto)), HttpStatus.OK);
    }

    @DeleteMapping("/{subcategoryId}")
    public ResponseEntity<ApiResponse> deleteSubCategory(@PathVariable final Long subcategoryId) {
        subcategoryService.deleteSubcategoryById(subcategoryId);
        return ResponseEntity.noContent().build();
    }
}
