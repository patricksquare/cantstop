package hr.pios.cantstop.controller;

import hr.pios.cantstop.dto.ApiResponse;
import hr.pios.cantstop.dto.CreateOrderProductDto;
import hr.pios.cantstop.dto.FilterOrderDto;
import hr.pios.cantstop.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/order")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class OrderController {

    private final OrderService orderService;


    @PostMapping("/all-orders")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ApiResponse> getAllOrders(@Param("page") final int page,
                                                    @Param("size") final int size,
                                                    @RequestBody(required = false) final FilterOrderDto filterOrderDto) {
        return new ResponseEntity<>(new ApiResponse(orderService.getAllOrders(page, size, filterOrderDto)), HttpStatus.OK);
    }

    @PostMapping("/current-user/all-orders")
    public ResponseEntity<ApiResponse> getAllCurrentUserOrders(@Param("page") final int page,
                                                               @Param("size") final int size,
                                                               @RequestBody(required = false) final FilterOrderDto filterOrderDto) {
        return new ResponseEntity<>(new ApiResponse(orderService.getAllCurrentUserOrders(page, size, filterOrderDto)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ApiResponse> createOrder(@RequestBody final CreateOrderProductDto createOrderProductDto) {
        return new ResponseEntity<>(new ApiResponse(orderService.createOrder(createOrderProductDto)), HttpStatus.CREATED);
    }

    @DeleteMapping("/{orderId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ApiResponse> deleteOrder(@PathVariable final Long orderId) {
        orderService.deleteOrder(orderId);
        return ResponseEntity.noContent().build();
    }
}
