package hr.pios.cantstop.controller;

import hr.pios.cantstop.dto.ApiResponse;
import hr.pios.cantstop.dto.FilterProductDto;
import hr.pios.cantstop.dto.ProductDto;
import hr.pios.cantstop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/product")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class ProductController {

    private final ProductService productService;


    @PostMapping("/all-products")
    public ResponseEntity<ApiResponse> getAllProducts(@Param("page") final int page,
                                                      @Param("size") final int size,
                                                      @RequestBody(required = false) final FilterProductDto filterProductDto) {
        return new ResponseEntity<>(new ApiResponse(productService.getAllProducts(page, size, filterProductDto)), HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ApiResponse> createProduct(@RequestBody final ProductDto productDto) {
        return new ResponseEntity<>(new ApiResponse(productService.createProduct(productDto)), HttpStatus.CREATED);
    }

    @PutMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ApiResponse> updateProduct(@RequestBody final ProductDto productDto) {
        return new ResponseEntity<>(new ApiResponse(productService.updateProduct(productDto)), HttpStatus.OK);
    }

    @DeleteMapping("/{productId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ApiResponse> deleteProduct(@PathVariable final Long productId) {
        productService.deleteProductById(productId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/max-price")
    public ResponseEntity<ApiResponse> getMaxProductPrice() {
        return new ResponseEntity<>(new ApiResponse(productService.getMaxProductPrice()), HttpStatus.OK);
    }
}
