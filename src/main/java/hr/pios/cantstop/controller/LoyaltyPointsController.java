package hr.pios.cantstop.controller;

import hr.pios.cantstop.dto.ApiResponse;
import hr.pios.cantstop.service.LoyaltyPointsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/loyalty-points")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class LoyaltyPointsController {

    private final LoyaltyPointsService loyaltyPointsService;


    @GetMapping("/{userId}")
    public ResponseEntity<ApiResponse> getUserLoyaltyPoints(@PathVariable final Long userId) {
        return new ResponseEntity<>(new ApiResponse(loyaltyPointsService.getUserLoyaltyPoints(userId)), HttpStatus.OK);
    }
}
