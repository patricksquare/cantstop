package hr.pios.cantstop.advice;

import hr.pios.cantstop.dto.ApiResponse;
import hr.pios.cantstop.exception.CategoryCreationException;
import hr.pios.cantstop.exception.NotEnoughLoyaltyPointsException;
import hr.pios.cantstop.exception.NotFoundException;
import hr.pios.cantstop.exception.OrderCreationException;
import hr.pios.cantstop.exception.ProductCreationException;
import hr.pios.cantstop.exception.RegistrationException;
import hr.pios.cantstop.exception.SubcategoryCreationException;
import liquibase.exception.DatabaseException;
import org.springframework.core.convert.ConversionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.NoSuchElementException;

@RestControllerAdvice
public class ExceptionAdvice {

    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<ApiResponse> handleNotFoundException(RuntimeException e) {
        return new ResponseEntity<>(new ApiResponse(e.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({IllegalArgumentException.class,
            NoSuchElementException.class,
            DatabaseException.class,
            BadCredentialsException.class,
            RegistrationException.class,
            ProductCreationException.class,
            CategoryCreationException.class,
            SubcategoryCreationException.class,
            OrderCreationException.class,
            NotEnoughLoyaltyPointsException.class})
    public ResponseEntity<ApiResponse> handleBadRequestException(RuntimeException e) {
        return new ResponseEntity<>(new ApiResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ConversionException.class})
    public ResponseEntity<ApiResponse> handleInternalServerErrorException(RuntimeException e) {
        return new ResponseEntity<>(new ApiResponse(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}