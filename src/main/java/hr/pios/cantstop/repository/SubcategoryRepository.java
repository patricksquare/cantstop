package hr.pios.cantstop.repository;

import hr.pios.cantstop.model.Subcategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubcategoryRepository extends JpaRepository<Subcategory, Long> {

    boolean existsById(Long id);

    boolean existsByName(String name);
}
