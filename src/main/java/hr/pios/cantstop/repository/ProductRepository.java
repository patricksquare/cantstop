package hr.pios.cantstop.repository;

import hr.pios.cantstop.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {

    Product findFirstByOrderByPriceDesc();

    boolean existsById(Long id);

    boolean existsByName(String name);
}
