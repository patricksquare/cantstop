package hr.pios.cantstop.repository;

import hr.pios.cantstop.model.LoyaltyPoints;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoyaltyPointsRepository extends JpaRepository<LoyaltyPoints, Long> {

    LoyaltyPoints findByUser_Id(Long userId);

    boolean existsByUser_Id(Long userId);
}
