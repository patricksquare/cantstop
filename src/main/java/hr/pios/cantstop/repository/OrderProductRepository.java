package hr.pios.cantstop.repository;

import hr.pios.cantstop.model.Order;
import hr.pios.cantstop.model.OrderProduct;
import hr.pios.cantstop.model.OrderProductId;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderProductRepository extends JpaRepository<OrderProduct, OrderProductId>, JpaSpecificationExecutor<OrderProduct> {

    List<OrderProduct> findAllByOrderProductId_Order_Id(Long orderId);

    void deleteAllByOrderProductId_Order_Id(Long orderId);
}
