package hr.pios.cantstop.support;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import hr.pios.cantstop.config.WebSecurityConfiguration;
import hr.pios.cantstop.jwt.JwtRequestFilter;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.core.annotation.AliasFor;

@Retention(RetentionPolicy.RUNTIME)
@WebMvcTest(excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {WebSecurityConfiguration.class, JwtRequestFilter.class}))
@AutoConfigureMockMvc(addFilters = false)
public @interface CantStopMvcTest {

    @AliasFor(annotation = WebMvcTest.class, attribute = "value")
    Class<?>[] value() default {};

    @AliasFor(annotation = WebMvcTest.class, attribute = "controllers")
    Class<?>[] controllers() default {};
}