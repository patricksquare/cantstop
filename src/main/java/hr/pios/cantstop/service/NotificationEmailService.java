package hr.pios.cantstop.service;

import hr.pios.cantstop.model.Order;
import hr.pios.cantstop.model.User;

public interface NotificationEmailService {

    void sendSuccessfulRegistrationEmail(String emailAddress);

    void sendSuccessfulOrderEmail(String emailAddress);
}
