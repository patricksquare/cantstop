package hr.pios.cantstop.service;

import hr.pios.cantstop.dto.LoyaltyPointsDto;
import hr.pios.cantstop.model.LoyaltyPoints;

public interface LoyaltyPointsService {

    LoyaltyPointsDto getUserLoyaltyPoints(Long userId);

    void increaseLoyaltyPoints(Long userid, Integer amount);

    void decreaseLoyaltyPoints(Long userId, Integer amount);

    Boolean existsByUserId(Long userId);

    Boolean hasEnoughLoyaltyPoints(Long userId, Integer amount);
}
