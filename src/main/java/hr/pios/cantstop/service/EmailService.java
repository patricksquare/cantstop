package hr.pios.cantstop.service;

import hr.pios.cantstop.model.Email;
import javax.mail.MessagingException;

public interface EmailService {

    void sendHtmlEmail(Email email) throws MessagingException;
}
