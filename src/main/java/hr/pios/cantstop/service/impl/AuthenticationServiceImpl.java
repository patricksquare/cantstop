package hr.pios.cantstop.service.impl;

import hr.pios.cantstop.converter.RoleConverter;
import hr.pios.cantstop.converter.UserConverter;
import hr.pios.cantstop.dto.LoginDto;
import hr.pios.cantstop.dto.RoleDto;
import hr.pios.cantstop.dto.UserDto;
import hr.pios.cantstop.exception.RegistrationException;
import hr.pios.cantstop.jwt.JwtResponse;
import hr.pios.cantstop.jwt.JwtTokenBuilder;
import hr.pios.cantstop.model.User;
import hr.pios.cantstop.repository.RoleRepository;
import hr.pios.cantstop.repository.UserRepository;
import hr.pios.cantstop.service.AuthenticationService;
import hr.pios.cantstop.service.NotificationEmailService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final AuthenticationManager authenticationManager;

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    private final UserConverter userConverter;
    private final RoleConverter roleConverter;

    private final NotificationEmailService notificationEmailService;


    @Override
    public JwtResponse login(final LoginDto loginDto) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
        }
        catch (AuthenticationException e) {
            throw new BadCredentialsException("Bad credentials", e);
        }

        User user = userRepository.findByUsername(loginDto.getUsername());

        return new JwtResponse(JwtTokenBuilder.generateToken(user));
    }

    @Override
    @Transactional
    public UserDto register(final UserDto userDto) {
        checkValidity(userDto);

        userDto.setPassword(new BCryptPasswordEncoder().encode(userDto.getPassword()));

        User user = userRepository.save(userConverter.convert(userDto));

        notificationEmailService.sendSuccessfulRegistrationEmail(user.getEmail());

        return userConverter.convert(user);
    }

    private void checkValidity(final UserDto userDto) {
        if (userRepository.existsByEmail(userDto.getEmail())) {
            throw new RegistrationException("Email address is already in use. Please use a different address.");
        }

        if (userRepository.existsByUsername(userDto.getUsername())) {
            throw new RegistrationException("Username already exists. Please use a different username.");
        }
    }

    @Override
    public List<RoleDto> getAllRoles() {
        return roleRepository.findAll().stream().map(roleConverter::convert).collect(Collectors.toList());
    }
}
