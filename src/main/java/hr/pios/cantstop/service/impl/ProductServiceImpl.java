package hr.pios.cantstop.service.impl;

import hr.pios.cantstop.converter.ProductConverter;
import hr.pios.cantstop.dto.FilterProductDto;
import hr.pios.cantstop.dto.ProductDto;
import hr.pios.cantstop.exception.NotFoundException;
import hr.pios.cantstop.exception.ProductCreationException;
import hr.pios.cantstop.repository.CategoryRepository;
import hr.pios.cantstop.repository.ProductRepository;
import hr.pios.cantstop.repository.SubcategoryRepository;
import hr.pios.cantstop.service.ProductService;
import hr.pios.cantstop.specification.ProductSpecification;
import java.math.BigDecimal;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final SubcategoryRepository subcategoryRepository;

    private final ProductConverter productConverter;


    @Override
    public Page<ProductDto> getAllProducts(final int page, final int size, final FilterProductDto filterProductDto) {
        Pageable pageable = PageRequest.of(page, size);

        return productRepository.findAll(new ProductSpecification(filterProductDto), pageable).map(productConverter::convert);
    }

    @Override
    public ProductDto createProduct(final ProductDto productDto) {
        validateProduct(productDto);

        return productConverter.convert(productRepository.save(productConverter.convert(productDto)));
    }

    @Override
    public ProductDto updateProduct(final ProductDto productDto) {
        if (!productRepository.existsById(productDto.getId())) {
            throw new NotFoundException("Product with ID " + productDto.getId() + " not found.");
        }
        validateProduct(productDto);

        return productConverter.convert(productRepository.save(productConverter.convert(productDto)));
    }

    @Override
    @Transactional
    public void deleteProductById(final Long productId) {
        if (!productRepository.existsById(productId)) {
            throw new NotFoundException("Product with ID " + productId + " not found.");
        }
        productRepository.deleteById(productId);
    }

    @Override
    public BigDecimal getMaxProductPrice() {
        return productRepository.findFirstByOrderByPriceDesc().getPrice();
    }

    private void validateProduct(final ProductDto productDto) {
        if (productRepository.existsByName(productDto.getName())) {
            throw new ProductCreationException("Product with name " + productDto.getName() + " already exists");
        }

        if (!categoryRepository.existsById(productDto.getCategoryDto().getId())) {
            throw new NotFoundException("Category with ID " + productDto.getCategoryDto().getId() + " not found.");
        }

        if (productDto.getSubcategoryDto() != null && !subcategoryRepository.existsById(productDto.getSubcategoryDto().getId())) {
            throw new NotFoundException("Subcategory with ID " + productDto.getSubcategoryDto().getId() + " not found.");
        }
    }
}
