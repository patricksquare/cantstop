package hr.pios.cantstop.service.impl;

import hr.pios.cantstop.converter.CategoryConverter;
import hr.pios.cantstop.dto.CategoryDto;
import hr.pios.cantstop.exception.CategoryCreationException;
import hr.pios.cantstop.exception.NotFoundException;
import hr.pios.cantstop.repository.CategoryRepository;
import hr.pios.cantstop.service.CategoryService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final CategoryConverter categoryConverter;


    @Override
    public List<CategoryDto> getAllCategories() {
        return categoryRepository.findAll().stream().map(categoryConverter::convert).collect(Collectors.toList());
    }

    @Override
    public CategoryDto createCategory(final CategoryDto categoryDto) {
        validateCategory(categoryDto);

        return categoryConverter.convert(categoryRepository.save(categoryConverter.convert(categoryDto)));
    }

    @Override
    public CategoryDto updateCategory(final CategoryDto categoryDto) {
        if (!categoryRepository.existsById(categoryDto.getId())) {
            throw new NotFoundException("Category with ID " + categoryDto.getId() + " not found.");
        }
        validateCategory(categoryDto);

        return categoryConverter.convert(categoryRepository.save(categoryConverter.convert(categoryDto)));
    }

    @Override
    @Transactional
    public void deleteCategoryById(final Long categoryId) {
        if (!categoryRepository.existsById(categoryId)) {
            throw new NotFoundException("Category with ID " + categoryId + " not found.");
        }
        categoryRepository.deleteById(categoryId);
    }

    private void validateCategory(final CategoryDto categoryDto) {
        if (categoryRepository.existsByName(categoryDto.getName())) {
            throw new CategoryCreationException("Category with name " + categoryDto.getName() + " already exists");
        }
    }
}
