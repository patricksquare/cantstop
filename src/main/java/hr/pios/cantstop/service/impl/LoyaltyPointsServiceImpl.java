package hr.pios.cantstop.service.impl;

import hr.pios.cantstop.converter.LoyaltyPointsConverter;
import hr.pios.cantstop.dto.LoyaltyPointsDto;
import hr.pios.cantstop.exception.NotEnoughLoyaltyPointsException;
import hr.pios.cantstop.exception.NotFoundException;
import hr.pios.cantstop.model.LoyaltyPoints;
import hr.pios.cantstop.repository.LoyaltyPointsRepository;
import hr.pios.cantstop.repository.UserRepository;
import hr.pios.cantstop.service.LoyaltyPointsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class LoyaltyPointsServiceImpl implements LoyaltyPointsService {

    private final LoyaltyPointsRepository loyaltyPointsRepository;
    private final UserRepository userRepository;

    private final LoyaltyPointsConverter loyaltyPointsConverter;


    @Override
    public LoyaltyPointsDto getUserLoyaltyPoints(Long userId) {
        if (!existsByUserId(userId)) {
            throw new NotFoundException("Loyalty points for user with ID " + userId + " not found.");
        }

        return loyaltyPointsConverter.convert(loyaltyPointsRepository.findByUser_Id(userId));
    }

    @Override
    @Transactional
    public void increaseLoyaltyPoints(final Long userId, final Integer amount) {
        LoyaltyPoints loyaltyPoints = new LoyaltyPoints();

        if (existsByUserId(userId)) {
            loyaltyPoints = loyaltyPointsRepository.findByUser_Id(userId);
            loyaltyPoints.setPoints(loyaltyPoints.getPoints() + amount);
        }
        else {
            loyaltyPoints.setPoints(amount);
            loyaltyPoints.setUser(userRepository.findById(userId)
                    .orElseThrow(() -> new NotFoundException("User with ID " + userId + " not found.")));
        }

        loyaltyPointsRepository.save(loyaltyPoints);
    }

    @Override
    @Transactional
    public void decreaseLoyaltyPoints(final Long userId, final Integer amount) {
        if (existsByUserId(userId)) {
            LoyaltyPoints loyaltyPoints = loyaltyPointsRepository.findByUser_Id(userId);

            if (!hasEnoughLoyaltyPoints(userId, amount)) {
                throw new NotEnoughLoyaltyPointsException("User with ID " + userId + " does not have enough loyalty points");
            }

            loyaltyPoints.setPoints(loyaltyPoints.getPoints() - amount);

            loyaltyPointsRepository.save(loyaltyPoints);
        }
    }

    @Override
    public Boolean existsByUserId(final Long userId) {
        return loyaltyPointsRepository.existsByUser_Id(userId);
    }

    @Override
    public Boolean hasEnoughLoyaltyPoints(final Long userId, final Integer amount) {
        if (existsByUserId(userId)) {
            return loyaltyPointsRepository.findByUser_Id(userId).getPoints() >= amount;
        }

        return false;
    }
}
