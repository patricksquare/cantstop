package hr.pios.cantstop.service.impl;

import hr.pios.cantstop.converter.OrderProductConverter;
import hr.pios.cantstop.dto.CreateOrderProductDto;
import hr.pios.cantstop.dto.FilterOrderDto;
import hr.pios.cantstop.dto.OrderProductDto;
import hr.pios.cantstop.exception.NotFoundException;
import hr.pios.cantstop.exception.OrderCreationException;
import hr.pios.cantstop.model.Order;
import hr.pios.cantstop.model.OrderProduct;
import hr.pios.cantstop.model.OrderProductId;
import hr.pios.cantstop.model.Product;
import hr.pios.cantstop.repository.OrderProductRepository;
import hr.pios.cantstop.repository.OrderRepository;
import hr.pios.cantstop.repository.ProductRepository;
import hr.pios.cantstop.repository.UserRepository;
import hr.pios.cantstop.service.CurrentUserService;
import hr.pios.cantstop.service.LoyaltyPointsService;
import hr.pios.cantstop.service.NotificationEmailService;
import hr.pios.cantstop.service.OrderService;
import hr.pios.cantstop.specification.OrderProductSpecification;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final CurrentUserService currentUserService;
    private final LoyaltyPointsService loyaltyPointsService;
    private final NotificationEmailService notificationEmailService;

    private final OrderProductRepository orderProductRepository;
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    private final OrderProductConverter orderProductConverter;


    @Override
    public Page<OrderProductDto> getAllOrders(final int page, final int size, final FilterOrderDto filterOrderDto) {
        Pageable pageable = PageRequest.of(page, size);

        return orderProductRepository.findAll(new OrderProductSpecification(filterOrderDto), pageable).map(orderProductConverter::convert);
    }

    @Override
    public Page<OrderProductDto> getAllCurrentUserOrders(final int page, final int size, final FilterOrderDto filterOrderDto) {
        Pageable pageable = PageRequest.of(page, size);
        filterOrderDto.setUserId(currentUserService.getLoggedInUser().getId());

        return orderProductRepository.findAll(new OrderProductSpecification(filterOrderDto), pageable).map(orderProductConverter::convert);
    }

    @Override
    public List<OrderProductDto> createOrder(final CreateOrderProductDto createOrderProductDto) {
        if (createOrderProductDto.getUserId() == null) {
            createOrderProductDto.setUserId(currentUserService.getLoggedInUser().getId());
        }

        if (createOrderProductDto.getDiscount() != null &&
                !loyaltyPointsService.hasEnoughLoyaltyPoints(createOrderProductDto.getUserId(), createOrderProductDto.getDiscount())) {
            throw new OrderCreationException("User does not have enough loyalty points.");
        }

        BigDecimal totalPrice = calculateTotalPrice(createOrderProductDto);
        Order order = orderRepository.save(buildOrder(createOrderProductDto.getUserId(), totalPrice, createOrderProductDto.getDiscount()));

        updateLoyaltyPoints(createOrderProductDto, order.getFinalPrice());

        for (Map.Entry<Long, Integer> entry : createOrderProductDto.getProductMap().entrySet()) {
            OrderProductId orderProductId = new OrderProductId(order, getProduct(entry.getKey()));
            orderProductRepository.save(new OrderProduct(orderProductId, entry.getValue()));
        }

        notificationEmailService.sendSuccessfulOrderEmail(order.getUser().getEmail());

        List<OrderProductDto> orderProductDtos = new ArrayList<>();

        for (OrderProduct orderProduct : orderProductRepository.findAllByOrderProductId_Order_Id(order.getId())) {
            orderProductDtos.add(orderProductConverter.convert(orderProduct));
        }

        return  orderProductDtos;
    }

    @Override
    @Transactional
    public void deleteOrder(Long orderId) {
        if (!orderRepository.existsById(orderId)) {
            throw new NotFoundException("Order with ID " + orderId + " not found.");
        }

        orderProductRepository.deleteAllByOrderProductId_Order_Id(orderId);
        orderRepository.deleteById(orderId);
    }

    private BigDecimal calculateTotalPrice(final CreateOrderProductDto createOrderProductDto) {
        BigDecimal totalPrice = new BigDecimal(0);

        for (Map.Entry<Long, Integer> entry : createOrderProductDto.getProductMap().entrySet()) {
            totalPrice = totalPrice.add(getProduct(entry.getKey()).getPrice().multiply(BigDecimal.valueOf(entry.getValue())));
        }

        return totalPrice;
    }

    private Product getProduct(Long productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new NotFoundException("Product with ID " + productId + " not found."));
    }

    private Order buildOrder(Long userId, BigDecimal totalPrice, Integer discount) {
        return Order.builder()
                .date(LocalDate.now())
                .totalPrice(totalPrice)
                .discount(discount)
                .finalPrice(discount != null ? totalPrice.subtract(BigDecimal.valueOf(discount)) : totalPrice)
                .user(userRepository.findById(userId).orElseThrow(() -> new NotFoundException("User with ID " + userId + " not found.")))
                .build();
    }

    private void updateLoyaltyPoints(final CreateOrderProductDto createOrderProductDto, final BigDecimal finalPrice) {
        if (createOrderProductDto.getDiscount() != null) {
            loyaltyPointsService.decreaseLoyaltyPoints(createOrderProductDto.getUserId(), createOrderProductDto.getDiscount());
        }
        loyaltyPointsService.increaseLoyaltyPoints(createOrderProductDto.getUserId(), finalPrice.intValue()/100);
    }
}
