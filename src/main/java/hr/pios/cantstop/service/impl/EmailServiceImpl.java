package hr.pios.cantstop.service.impl;

import hr.pios.cantstop.model.Email;
import hr.pios.cantstop.service.EmailService;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;

    @Override
    public void sendHtmlEmail(Email email) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");

        setEmailMessageParameters(email, message);

        javaMailSender.send(mimeMessage);
    }

    private void setEmailMessageParameters(Email email, MimeMessageHelper message) throws MessagingException {
        if (email.getSender() != null) {
            message.setFrom(email.getSender());
        }
        message.setTo(email.getReceivers());
        if (email.getBcc() != null) {
            message.setBcc(email.getBcc());
        }
        message.setSubject(email.getSubject());
        message.setText(email.getText(), true);
    }
}