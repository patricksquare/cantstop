package hr.pios.cantstop.service.impl;

import hr.pios.cantstop.model.Role;
import hr.pios.cantstop.model.User;
import hr.pios.cantstop.model.codebook.Roles;
import hr.pios.cantstop.repository.UserRepository;
import hr.pios.cantstop.service.CurrentUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CurrentUserServiceImpl implements CurrentUserService {

    private final UserRepository userRepository;

    @Override
    public String getLoggedInUserUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        return authentication.getName();
    }

    @Override
    public Role getLoggedInUserRole() {
        User loggedInUser = userRepository.findByUsername(getLoggedInUserUsername());

        return loggedInUser.getRole();
    }

    @Override
    public User getLoggedInUser() {
        return userRepository.findByUsername(getLoggedInUserUsername());
    }

    @Override
    public boolean isLoggedInUserAdmin() {
        return Roles.ROLE_ADMIN.getRole().equals(getLoggedInUserRole().getName());
    }
}
