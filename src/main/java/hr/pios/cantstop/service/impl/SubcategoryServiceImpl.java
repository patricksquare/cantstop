package hr.pios.cantstop.service.impl;

import hr.pios.cantstop.converter.SubcategoryConverter;
import hr.pios.cantstop.dto.SubcategoryDto;
import hr.pios.cantstop.exception.NotFoundException;
import hr.pios.cantstop.exception.SubcategoryCreationException;
import hr.pios.cantstop.repository.SubcategoryRepository;
import hr.pios.cantstop.service.SubcategoryService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class SubcategoryServiceImpl implements SubcategoryService {

    private final SubcategoryRepository subcategoryRepository;
    private final SubcategoryConverter subcategoryConverter;


    @Override
    public List<SubcategoryDto> getAllSubcategories() {
        return subcategoryRepository.findAll().stream().map(subcategoryConverter::convert).collect(Collectors.toList());
    }

    @Override
    public SubcategoryDto createSubcategory(final SubcategoryDto subcategoryDto) {
        validateSubcategory(subcategoryDto);

        return subcategoryConverter.convert(subcategoryRepository.save(subcategoryConverter.convert(subcategoryDto)));
    }

    @Override
    public SubcategoryDto updateSubcategory(final SubcategoryDto subcategoryDto) {
        if (!subcategoryRepository.existsById(subcategoryDto.getId())) {
            throw new NotFoundException("Category with ID " + subcategoryDto.getId() + " not found.");
        }
        validateSubcategory(subcategoryDto);

        return subcategoryConverter.convert(subcategoryRepository.save(subcategoryConverter.convert(subcategoryDto)));
    }

    @Override
    @Transactional
    public void deleteSubcategoryById(final Long subcategoryId) {
        if (!subcategoryRepository.existsById(subcategoryId)) {
            throw new NotFoundException("Category with ID " + subcategoryId + " not found.");
        }
        subcategoryRepository.deleteById(subcategoryId);
    }

    private void validateSubcategory(final SubcategoryDto subcategoryDto) {
        if (subcategoryRepository.existsByName(subcategoryDto.getName())) {
            throw new SubcategoryCreationException("Subcategory with name " + subcategoryDto.getName() + " already exists");
        }
    }
}
