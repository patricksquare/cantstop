package hr.pios.cantstop.service.impl;

import hr.pios.cantstop.model.Email;
import hr.pios.cantstop.model.Order;
import hr.pios.cantstop.model.User;
import hr.pios.cantstop.service.EmailService;
import hr.pios.cantstop.service.NotificationEmailService;
import javax.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class NotificationEmailServiceImpl implements NotificationEmailService {

    private final EmailService emailService;


    @Override
    public void sendSuccessfulRegistrationEmail(String emailAddress) {
        final String emailText = "<p>You have successfully completed your registration on the CantStop web shop application.</p>";

        Email email = Email.builder()
                .sender("cantstop-webshop@outlook.com")
                .receivers(new String[] {emailAddress})
                .subject("Successful registration - NO REPLY")
                .text(emailText)
                .build();

        try {
            emailService.sendHtmlEmail(email);
        }
        catch (MessagingException e) {
            log.error("Error while trying to send registration email", e);
        }
    }

    @Override
    public void sendSuccessfulOrderEmail(String emailAddress) {
        String emailText = "<p>You have successfully completed your order on the CantStop web shop application.</p>" +
                "<p>For more details visit the CantStop web shop application</p>";

        Email email = Email.builder()
                .sender("cantstop-webshop@outlook.com")
                .receivers(new String[] {emailAddress})
                .subject("Successful order - NO REPLY")
                .text(emailText)
                .build();

        try {
            emailService.sendHtmlEmail(email);
        }
        catch (MessagingException e) {
            log.error("Error while trying to send order email", e);
        }
    }
}
