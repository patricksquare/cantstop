package hr.pios.cantstop.service;

import hr.pios.cantstop.dto.FilterProductDto;
import hr.pios.cantstop.dto.ProductDto;
import java.math.BigDecimal;
import org.springframework.data.domain.Page;

public interface ProductService {

    Page<ProductDto> getAllProducts(int page, int size, FilterProductDto filterProductDto);

    ProductDto createProduct(ProductDto productDto);

    ProductDto updateProduct(ProductDto productDto);

    void deleteProductById(Long productId);

    BigDecimal getMaxProductPrice();
}
