package hr.pios.cantstop.service;

import hr.pios.cantstop.model.Role;
import hr.pios.cantstop.model.User;

public interface CurrentUserService {

    String getLoggedInUserUsername();

    Role getLoggedInUserRole();

    User getLoggedInUser();

    boolean isLoggedInUserAdmin();
}
