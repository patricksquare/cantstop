package hr.pios.cantstop.service;

import hr.pios.cantstop.dto.LoginDto;
import hr.pios.cantstop.dto.RoleDto;
import hr.pios.cantstop.dto.UserDto;
import hr.pios.cantstop.jwt.JwtResponse;

import java.util.List;

public interface AuthenticationService {

    JwtResponse login(LoginDto loginDto);

    UserDto register(UserDto userDto);

    List<RoleDto> getAllRoles();
}
