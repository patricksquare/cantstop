package hr.pios.cantstop.service;

import hr.pios.cantstop.dto.SubcategoryDto;
import java.util.List;

public interface SubcategoryService {

    List<SubcategoryDto> getAllSubcategories();

    SubcategoryDto createSubcategory(SubcategoryDto SubcategoryDto);

    SubcategoryDto updateSubcategory(SubcategoryDto SubcategoryDto);

    void deleteSubcategoryById(Long SubcategoryId);
}
