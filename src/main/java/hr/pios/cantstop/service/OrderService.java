package hr.pios.cantstop.service;

import hr.pios.cantstop.dto.CreateOrderProductDto;
import hr.pios.cantstop.dto.FilterOrderDto;
import hr.pios.cantstop.dto.OrderProductDto;
import java.util.List;
import org.springframework.data.domain.Page;

public interface OrderService {

    Page<OrderProductDto> getAllOrders(int page, int size, FilterOrderDto filterOrderDto);

    Page<OrderProductDto> getAllCurrentUserOrders(int page, int size, FilterOrderDto filterOrderDto);

    List<OrderProductDto> createOrder(CreateOrderProductDto createOrderProductDto);

    void deleteOrder(Long orderId);
}
