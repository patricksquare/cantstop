package hr.pios.cantstop.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class OrderDto {

    private Long id;
    private BigDecimal totalPrice;
    private Integer discount;
    private BigDecimal finalPrice;
    private LocalDate date;
    private UserDto userDto;
}
