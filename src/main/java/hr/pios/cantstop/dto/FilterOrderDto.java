package hr.pios.cantstop.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FilterOrderDto {

    private Long userId;
    private Long orderId;
    private Long productId;
    private BigDecimal minPrice;
    private BigDecimal maxPrice;
    private LocalDate dateFrom;
    private LocalDate dateTo;
}
