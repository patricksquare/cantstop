package hr.pios.cantstop.dto;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FilterProductDto {

    private String name;
    private BigDecimal maxPrice;
    private BigDecimal minPrice;
    private Integer categoryId;
    private Integer subcategoryId;
    private String orderField;
    private String orderType;
}
