package hr.pios.cantstop.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private Long id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private String email;
    private RoleDto roleDto;

    public UserDto(String username, String password, RoleDto roleDto) {
        this.username = username;
        this.password = password;
        this.roleDto = roleDto;
    }
}

