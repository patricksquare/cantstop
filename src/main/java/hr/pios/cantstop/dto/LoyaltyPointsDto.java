package hr.pios.cantstop.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoyaltyPointsDto {

    private Long userId;
    private Integer loyaltyPoints;
}
