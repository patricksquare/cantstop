package hr.pios.cantstop.dto;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateOrderProductDto {

    private Map<Long, Integer> productMap = new HashMap<>();
    private Integer discount;
    private Long userId;
}
