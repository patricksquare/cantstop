package hr.pios.cantstop.specification;

import hr.pios.cantstop.dto.FilterProductDto;
import hr.pios.cantstop.model.Product;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

@RequiredArgsConstructor
public class ProductSpecification implements Specification<Product> {

    private final FilterProductDto filter;


    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if (filter != null) {
            if (filter.getName() != null) {
                predicates.add(criteriaBuilder.like(root.get("name"), "%" + filter.getName() + "%"));
            }

            if (filter.getMinPrice() != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("price"), filter.getMinPrice()));
            }

            if (filter.getMaxPrice() != null) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("price"), filter.getMaxPrice()));
            }

            if (filter.getCategoryId() != null) {
                predicates.add(criteriaBuilder.equal(root.get("category").get("id"), filter.getCategoryId()));
            }

            if (filter.getSubcategoryId() != null) {
                predicates.add(criteriaBuilder.equal(root.get("subcategory").get("id"), filter.getSubcategoryId()));
            }

            if (filter.getOrderField() != null && filter.getOrderType()!= null) {
                if (filter.getOrderType().equals("desc")) {
                    criteriaQuery.orderBy(criteriaBuilder.desc(root.get(filter.getOrderField())));
                }
                else if (filter.getOrderType().equals("asc")) {
                    criteriaQuery.orderBy(criteriaBuilder.asc(root.get(filter.getOrderField())));
                }
            }
        }
        criteriaQuery.where(predicates.toArray((new Predicate[0])));

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
