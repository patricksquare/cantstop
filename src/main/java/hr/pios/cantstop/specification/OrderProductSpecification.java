package hr.pios.cantstop.specification;

import hr.pios.cantstop.dto.FilterOrderDto;
import hr.pios.cantstop.model.OrderProduct;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

@RequiredArgsConstructor
public class OrderProductSpecification implements Specification<OrderProduct> {

    private final FilterOrderDto filter;


    @Override
    public Predicate toPredicate(Root<OrderProduct> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if (filter != null) {
            if (filter.getUserId() != null) {
                predicates.add(criteriaBuilder.equal(root.get("orderProductId").get("order").get("user").get("id"), filter.getUserId()));
            }

            if (filter.getOrderId() != null) {
                predicates.add(criteriaBuilder.equal(root.get("orderProductId").get("order").get("id"), filter.getOrderId()));
            }

            if (filter.getProductId() != null) {
                predicates.add(criteriaBuilder.equal(root.get("orderProductId").get("product").get("id"), filter.getProductId()));
            }

            if (filter.getDateFrom() != null && filter.getDateTo() != null) {
                predicates.add(criteriaBuilder.between(root.get("orderProductId").get("order").get("date"), filter.getDateFrom(), filter.getDateTo()));
            }

            if (filter.getMinPrice() != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("orderProductId").get("product").get("price"), filter.getMinPrice()));
            }

            if (filter.getMaxPrice() != null) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("orderProductId").get("product").get("price"), filter.getMaxPrice()));
            }
        }
        criteriaQuery.where(predicates.toArray((new Predicate[0])));
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("orderProductId").get("order").get("date")));

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
