package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.OrderProductDto;
import hr.pios.cantstop.dto.ProductDto;
import hr.pios.cantstop.exception.NotFoundException;
import hr.pios.cantstop.model.OrderProduct;
import hr.pios.cantstop.repository.OrderProductRepository;
import hr.pios.cantstop.repository.OrderRepository;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class OrderProductConverter implements Converter<OrderProduct, OrderProductDto> {

    private final OrderRepository orderRepository;
    private final OrderProductRepository orderProductRepository;

    private final OrderConverter orderConverter;
    private final ProductConverter productConverter;


    @Override
    public OrderProductDto convert(OrderProduct orderProduct) {
        return OrderProductDto.builder()
                .orderDto(orderConverter.convert(orderRepository.findById(orderProduct.getOrderProductId().getOrder().getId())
                        .orElseThrow(() -> new NotFoundException("Order with ID " + orderProduct.getOrderProductId().getOrder().getId() + " not found."))))
                .productMap(getProductMap(orderProduct.getOrderProductId().getOrder().getId()))
                .build();
    }

    private Map<ProductDto, Integer> getProductMap(final Long orderId) {
        List<OrderProduct> orderProducts = orderProductRepository.findAllByOrderProductId_Order_Id(orderId);

        return orderProducts.stream()
                .collect(Collectors.toMap(orderProduct -> productConverter.convert(orderProduct.getOrderProductId().getProduct()), OrderProduct::getQuantity));
    }
}
