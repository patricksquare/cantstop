package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.UserDto;
import hr.pios.cantstop.model.User;
import hr.pios.cantstop.repository.LoyaltyPointsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class UserConverter implements Converter<User, UserDto> {

    private final RoleConverter roleConverter;
    private final LoyaltyPointsRepository loyaltyPointsRepository;

    @Override
    public UserDto convert(User user) {
        return UserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .dateOfBirth(user.getDateOfBirth())
                .roleDto(roleConverter.convert(user.getRole()))
                .build();
    }

    public User convert(UserDto userDto) {
        return User.builder()
                .id(userDto.getId())
                .username(userDto.getUsername())
                .password(userDto.getPassword())
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .email(userDto.getEmail())
                .dateOfBirth(userDto.getDateOfBirth())
                .role(roleConverter.convert(userDto.getRoleDto()))
                .build();
    }
}
