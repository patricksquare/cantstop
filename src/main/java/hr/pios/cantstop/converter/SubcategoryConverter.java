package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.SubcategoryDto;
import hr.pios.cantstop.model.Subcategory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class SubcategoryConverter implements Converter<Subcategory, SubcategoryDto> {

    @Override
    public SubcategoryDto convert(Subcategory subcategory) {
    return SubcategoryDto.builder()
            .id(subcategory.getId())
            .name(subcategory.getName())
            .build();
    }

    public Subcategory convert(SubcategoryDto subcategoryDto) {
    return Subcategory.builder()
            .id(subcategoryDto.getId())
            .name(subcategoryDto.getName())
            .build();
    }
}
