package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.OrderDto;
import hr.pios.cantstop.model.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class OrderConverter implements Converter<Order, OrderDto> {

    private final UserConverter userConverter;


    @Override
    public OrderDto convert(Order order) {
        return OrderDto.builder()
                .id(order.getId() != null ? order.getId() : null)
                .totalPrice(order.getTotalPrice())
                .discount(order.getDiscount())
                .finalPrice(order.getFinalPrice())
                .date(order.getDate())
                .userDto(userConverter.convert(order.getUser()))
                .build();
    }

    public Order convert(OrderDto orderDto) {
        return Order.builder()
                .id(orderDto.getId())
                .totalPrice(orderDto.getTotalPrice())
                .discount(orderDto.getDiscount())
                .finalPrice(orderDto.getFinalPrice())
                .date(orderDto.getDate())
                .user(userConverter.convert(orderDto.getUserDto()))
                .build();
    }
}
