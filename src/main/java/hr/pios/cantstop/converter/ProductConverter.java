package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.ProductDto;
import hr.pios.cantstop.model.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ProductConverter implements Converter<Product, ProductDto> {

    private final CategoryConverter categoryConverter;
    private final SubcategoryConverter subcategoryConverter;

    @Override
    public ProductDto convert(Product product) {
        return ProductDto.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .picture(product.getPicture())
                .categoryDto(categoryConverter.convert(product.getCategory()))
                .subcategoryDto(subcategoryConverter.convert(product.getSubcategory()))
                .build();
    }

    public Product convert(ProductDto productDto) {
        return Product.builder()
                .id(productDto.getId())
                .name(productDto.getName())
                .description(productDto.getDescription())
                .price(productDto.getPrice())
                .picture(productDto.getPicture())
                .category(categoryConverter.convert(productDto.getCategoryDto()))
                .subcategory(subcategoryConverter.convert(productDto.getSubcategoryDto()))
                .build();
    }
}
