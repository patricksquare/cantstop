package hr.pios.cantstop.converter;

import hr.pios.cantstop.dto.LoyaltyPointsDto;
import hr.pios.cantstop.model.LoyaltyPoints;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class LoyaltyPointsConverter implements Converter<LoyaltyPoints, LoyaltyPointsDto> {

    @Override
    public LoyaltyPointsDto convert(LoyaltyPoints loyaltyPoints) {
        return LoyaltyPointsDto.builder()
                .userId(loyaltyPoints.getUser().getId())
                .loyaltyPoints(loyaltyPoints.getPoints())
                .build();
    }
}
